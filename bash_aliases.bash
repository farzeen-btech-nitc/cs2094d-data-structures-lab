
function c() {
    build_dir="./.c.build"
    source_file="$1"
    exe_file="$build_dir/${source_file%.c}"

    mkdir -p $build_dir

    if [ ! \( -f "$exe_file" \) -o "$source_file" -nt "$exe_file" ]; then
        gcc -g "$source_file" -o "$exe_file"
        if [ $? -ne 0 ]; then return; fi
    fi
    shift;
    ./$exe_file $*
}

function c_clean() {
    rm -rf "./.c.build"
}
