#define main _main
#include "../06_dynamic_bst.c"
#undef main


int main() {
    int a[10] = {1,2,3,4,6,7,8};
    int b[20] = {5,10,11,12};

    merge(a, 8, b, 3);

    for(int i = 0; i<11; i++) {
        printf("%d ", b[i]);
    }
    printf("\n");
}
