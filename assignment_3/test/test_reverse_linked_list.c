#define main _main
#include "../03_binomial_heap.c"
#undef main

int main() {
    Node *head = make_node();
    Node *cur = head;
    for(int i = 0; i<10; i++) {
        cur->right_sibling = make_node();
        cur = cur->right_sibling;
        cur->x = i;
    }

    Node *last = reverse_node_list(&NULL_NODE, head);

    cur = last;
    int i = 0;
    while(cur != &NULL_NODE) {
        printf("%d ", cur->x);
        cur = cur->right_sibling;
        if(i++ == 20) break;
    }
    printf("\n");
}