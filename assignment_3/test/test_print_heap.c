#define main _main
#include "../03_binomial_heap.c"
#undef main

int main() {
    output_file = stdout;

    /*
        100 -- 200 ---  300
                |      /   |
               201    301 303
                       |
                      302
    */

    Heap *heap = make_heap();
    heap->roots = make_node();
    heap->roots->x = 100;

    Node *h1 = heap->roots->right_sibling = make_node();
    h1->x = 200;
    Node *h10 = h1->left_child = make_node();
    h10->x = 201;

    Node *h2 = h1->right_sibling = make_node();
    h2->x = 300;

    Node *h21 = h2->left_child = make_node();
    h21->x = 301;
    Node *h210 = h21->left_child = make_node();
    h210->x = 302;

    Node *h20 = h21->right_sibling = make_node();
    h20->x = 303;

    print_heap(heap);
    // print_level(heap->roots, 2);
}