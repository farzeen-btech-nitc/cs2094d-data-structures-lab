#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node {
    int x;
    struct Node *parent;
    struct Node *left_child;
    struct Node *right_sibling;
    int degree;
};
typedef struct Node Node;

Node NULL_NODE = {INT_MAX, &NULL_NODE, &NULL_NODE, &NULL_NODE, INT_MAX};

struct Heap {
    Node *roots;
};
typedef struct Heap Heap;


Heap *make_heap();
void insert(Heap *h, int x);
Node *minimum(Heap *h);
Node *extract_min(Heap *h);
Heap *union_heap(Heap *h1, Heap *h2);
void delete (Heap *h, int x);
void print_heap(Heap *h);


FILE *output_file;
FILE *input_file;

int main() {
    output_file = stdout;
    input_file = stdin;

    Heap *heap = make_heap();

    char buf[1024];
    while (fgets(buf, 1024, input_file) != NULL) {
        int x;
        Node *min;
        switch (buf[0]) {
        case 'i':
            x = atoi(buf + 1);
            insert(heap, x);
            break;

        case 'd':
            x = atoi(buf + 1);
            delete (heap, x);
            break;

        case 'm':
            min = minimum(heap);
            if (min != &NULL_NODE) {
                fprintf(output_file, "%d\n", min->x);
            } else {
                fprintf(output_file, "EMPTY\n");
            }
            break;

        case 'p':
            print_heap(heap);
            break;

        case 'x':
            min = extract_min(heap);
            if (min == &NULL_NODE) {
                fprintf(output_file, "EMPTY\n");
            } else {
                free(min);
            }
            break;

        case 's':
            free(heap);
            return 0;

        default:
            fprintf(output_file, "Unexpected input: \n");
            fprintf(output_file, "'%s'\n", buf);
            return -1;
        }
    }
    return 0;
}


Node *make_node() {
    Node *ret = malloc(sizeof(Node));
    ret->x = -1;
    ret->degree = 0;
    ret->parent = &NULL_NODE;
    ret->left_child = &NULL_NODE;
    ret->right_sibling = &NULL_NODE;
    return ret;
}

Heap *make_heap() {
    Heap *ret = malloc(sizeof(Heap));
    // Sentinel
    ret->roots = &NULL_NODE;
    return ret;
}

void insert(Heap *h, int x) {
    Heap *h1 = make_heap();
    h1->roots = make_node();
    h1->roots->x = x;
    Heap *h3 = union_heap(h, h1);
    h->roots = h3->roots;
    free(h1);
    free(h3);
}

Node *minimum(Heap *h) {
    Node *cur = h->roots;
    Node *min_node = cur;
    while (cur != &NULL_NODE) {
        if (cur->x < min_node->x) {
            min_node = cur;
        }
        cur = cur->right_sibling;
    }
    return min_node;
}

Node *reverse_node_list(Node *prev, Node *cur) {
    if (cur == &NULL_NODE)
        return prev;
    Node *next = cur->right_sibling;
    cur->right_sibling = prev;
    return reverse_node_list(cur, next);
}

Node *extract_min(Heap *h) {
    Node *min = minimum(h);
    if (min == &NULL_NODE)
        return min;

    Node *head_sentinel = make_node();
    head_sentinel->right_sibling = h->roots;
    h->roots = head_sentinel;

    Node *prev = h->roots;
    Node *cur = prev->right_sibling;
    while (cur != min) {
        prev = prev->right_sibling;
        cur = prev->right_sibling;
    }

    // unlink cur (min) from root list
    prev->right_sibling = cur->right_sibling;

    // New heap with children of min as roots
    Heap *h_temp = make_heap();
    h_temp->roots = reverse_node_list(&NULL_NODE, min->left_child);
    min->left_child = &NULL_NODE;

    // Degree of every node now has decreased by one
    cur = h_temp->roots;
    while (cur != &NULL_NODE) {
        cur->parent = &NULL_NODE;
        cur->degree -= 1;
        cur = cur->right_sibling;
    }

    h->roots = h->roots->right_sibling;
    free(head_sentinel);

    Heap *h_union = union_heap(h, h_temp);
    h->roots = h_union->roots;

    free(h_temp);
    free(h_union);

    return min;
}

Heap *merge_heap(Heap *h1, Heap *h2) {
    Node *cur1 = h1->roots;
    Node *cur2 = h2->roots;

    Heap *h3 = make_heap();
    // Note: h3->roots is now a sentinel
    h3->roots = make_node();
    Node *cur3 = h3->roots;

    while (cur1 != &NULL_NODE || cur2 != &NULL_NODE) {
        Node **small = cur1->degree <= cur2->degree ? &cur1 : &cur2;
        cur3->right_sibling = *small;
        cur3 = cur3->right_sibling;
        *small = (*small)->right_sibling;
    }

    // Remove the sentinel at the beginning
    Node *head_sentinel = h3->roots;
    h3->roots = h3->roots->right_sibling;
    free(head_sentinel);

    return h3;
}

void binomial_link(Node *n, Node *subnode) {
    subnode->parent = n;
    subnode->right_sibling = n->left_child;
    n->left_child = subnode;
    n->degree += 1;
}

void decrease_key(Node *n, int new_key) {
    assert(new_key < n->x);

    n->x = new_key;

    Node *cur = n;

    while (cur->parent != &NULL_NODE && cur->x < cur->parent->x) {
        int temp = cur->x;
        cur->x = cur->parent->x;
        cur->parent->x = temp;
        cur = cur->parent;
    }
}

Heap *union_heap(Heap *h1, Heap *h2) {
    Heap *h = merge_heap(h1, h2);
    Node *head_sentinel = make_node();
    head_sentinel->right_sibling = h->roots;
    h->roots = head_sentinel;

    Node *prev = h->roots;
    Node *cur = prev->right_sibling;
    Node *next = cur->right_sibling;

    while (cur != &NULL_NODE && next != &NULL_NODE) {
        if (cur->degree < next->degree) {
            goto continue_loop;
        }

        if (next->right_sibling != &NULL_NODE &&
            next->degree == next->right_sibling->degree) {
            goto continue_loop;
        }

        if (cur->x <= next->x) {
            // unlink next
            cur->right_sibling = next->right_sibling;
            binomial_link(cur, next);
        } else {
            // unlink cur
            prev->right_sibling = cur->right_sibling;
            binomial_link(next, cur);
        }

    continue_loop:
        prev = cur;
        cur = next;
        next = cur->right_sibling;
    }

    h->roots = head_sentinel->right_sibling;
    free(head_sentinel);
    return h;
}

Node *find_node(Node *n, int x) {
    if (n == &NULL_NODE)
        return n;

    if (n->x == x)
        return n;

    Node *left = find_node(n->left_child, x);
    if (left != &NULL_NODE)
        return left;

    return find_node(n->right_sibling, x);
}

void delete (Heap *h, int x) {
    Node *n = find_node(h->roots, x);
    if (n == &NULL_NODE) {
        fprintf(output_file, "NOT FOUND\n");
        return;
    }
    decrease_key(n, INT_MIN);
    extract_min(h);
}

int print_level(Node *n, int level) {
    if (n == &NULL_NODE)
        return 0;

    if (level == 1) {
        Node *cur = n;
        while (cur != &NULL_NODE) {
            fprintf(output_file, "%d ", cur->x);
            cur = cur->right_sibling;
        }
        return 1;
    } else {
        Node *cur = n;
        int ret = 0;
        ret |= print_level(cur->left_child, level - 1);
        ret |= print_level(cur->right_sibling, level);
        return ret;
    }
}

void print_heap(Heap *h) {
    int level = 1;
    int level_exists = 0;
    do {
        level_exists = print_level(h->roots, level);
        level++;
    } while (level_exists);
    fprintf(output_file, "\n");
}