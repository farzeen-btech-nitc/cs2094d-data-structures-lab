#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct DynamicArray {
    int *arr;
    int len;
    int cap;
};
typedef struct DynamicArray DynamicArray;


void append(DynamicArray *a, int x);
void concatenate(DynamicArray *a, DynamicArray *b);
int member(DynamicArray *a, int x);
int length(DynamicArray *a);
DynamicArray *slice(DynamicArray *a, int p, int q);
DynamicArray *from_str(char *s);
void print(DynamicArray *a);

FILE *output_file;
FILE *input_file;

int main() {
    output_file = stdout;
    input_file = stdin;

    char buf[1024];
    while (fgets(buf, 1024, input_file) != NULL) {
        int x, y;
        char *tok1, *tok2;
        DynamicArray *a, *b, *c;
        switch (buf[0]) {
        case 'a':
            strtok(buf, "()");
            tok1 = strtok(NULL, "()");
            tok2 = strtok(NULL, "()");

            a = from_str(tok1);
            x = atoi(tok2);
            append(a, x);
            print(a);
            break;

        case 'c':
            strtok(buf, "()");
            tok1 = strtok(NULL, "()");
            tok2 = strtok(NULL, "()");
            if(*tok2 == ' ') tok2 = strtok(NULL, "()");

            a = from_str(tok1);
            b = from_str(tok2);

            concatenate(a, b);
            print(a);
            break;

        case 'm':
            strtok(buf, "()");
            tok1 = strtok(NULL, "()");
            tok2 = strtok(NULL, "()");

            a = from_str(tok1);
            x = atoi(tok2);
            fprintf(output_file, "%d\n", member(a, x));
            break;

        case 'l':
            tok1 = strtok(buf + 1, "()");
            a = from_str(tok1);
            fprintf(output_file, "%d\n", length(a));
            break;

        case 's':
            tok1 = strtok(buf + 1, "()");
            tok2 = strtok(NULL, "()");
            a = from_str(tok1);
            tok1 = strtok(tok2, " ");
            tok2 = strtok(NULL, " ");
            x = atoi(tok1);
            y = atoi(tok2);
            x -= 1;
            y -= 1;
            c = slice(a, x, y);
            print(c);
            break;

        case 'x':
            return 0;

        default:
            fprintf(output_file, "Unexpected input: \n");
            fprintf(output_file, "'%s'\n", buf);
            return -1;
        }
    }
    return 0;
}

DynamicArray *from_str(char *s) {
    DynamicArray *a = malloc(sizeof(DynamicArray));
    a->arr = malloc(sizeof(int));
    a->len = 0;
    a->cap = 1;

    char *tok = strtok(s, " ");
    while (tok) {
        append(a, atoi(tok));
        tok = strtok(NULL, " ");
    }

    return a;
}

void append(DynamicArray *a, int x) {
    if (a->len == a->cap) {
        a->arr = realloc(a->arr, 2 * a->cap * sizeof(int));
        a->cap *= 2;
    }

    a->arr[a->len] = x;
    a->len += 1;
}

void concatenate(DynamicArray *a, DynamicArray *b) {
    for(int i = 0; i<b->len; i++) {
        append(a, b->arr[i]);
    }
}

int member(DynamicArray *a, int x) {
    for(int i = 0; i<a->len; i++) {
        if(a->arr[i] == x) return 1;
    }

    return 0;
}

int length(DynamicArray *a) {
    return a->len;
}

DynamicArray *slice(DynamicArray *a, int p, int q) {
    DynamicArray *b = malloc(sizeof(DynamicArray));
    b->cap = b->len = q-p+1;
    b->arr = malloc(b->cap*sizeof(int));
    memcpy(b->arr, a->arr+p, b->len);
    return b;
}

void print(DynamicArray *a) {
    for(int i = 0; i<a->len; i++) {
        fprintf(output_file, "%d ", a->arr[i]);
    }
    fprintf(output_file, "\n");
}
