#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

struct Node {
    int x;
    struct Node *parent;
    int rank;
};
typedef struct Node Node;

void makeset(int v);
void find(int x);
void union_set(int x, int y);
void count_access(); 

FILE* output_file;
FILE* input_file;

#define bool int
#define true 1
#define false 0

int main() {
    output_file = stdout;
    input_file = stdin;
    
    char buf[1024];
    while(fgets(buf, 1024, input_file) != NULL) {
        int x, y;
        char *x_str, *y_str;
        switch(buf[0]) {
            case 'm':
                x = atoi(buf+1);
                makeset(x);
                break;

            case 'f':
                x = atoi(buf+1);
                find(x);
                break;

            case 'u':
                strtok(buf, " ");
                x_str = strtok(NULL, " ");
                y_str = strtok(NULL, " ");
                x = atoi(x_str);
                y = atoi(y_str);
                union_set(x, y);
                break;

            case 's':
                count_access();
                return 0;

            default:
                fprintf(output_file, "Unexpected input: \n");
                fprintf(output_file, "'%s'\n", buf);
                return -1;
        }
    }
	return 0;
}

struct Sets{
    Node* nodes[10000];
    int count_access;
};
typedef struct Sets Sets;

Sets sets_none,
     sets_ru,
     sets_pc,
     sets_ru_pc;

// returns false if already exists
bool __makeset(Sets *sets, int x);
Node*  __find(Sets *sets, int x);
Node* __find_pc(Sets *sets, int x);

typedef Node* FindFn(Sets *, int);
Node* __union(Sets *sets, int x, int y, FindFn find_fn);
Node* __union_ru(Sets *sets, int x, int y, FindFn find_fn);


void makeset(int x) {
    bool m1 = __makeset(&sets_none, x);
    bool m2 = __makeset(&sets_ru, x);
    bool m3 = __makeset(&sets_pc, x);
    bool m4 = __makeset(&sets_ru_pc, x);
    assert(m1 == m2);
    assert(m2 == m3);
    assert(m3 == m4);

    if(!m1) {
        fprintf(output_file, "PRESENT\n");
    } else {
        fprintf(output_file, "%d\n", x);
    }
}

void find(int x) {
    Node* r1 = __find(&sets_none, x);
    Node* r2 = __find(&sets_ru, x);
    Node* r3 = __find_pc(&sets_pc, x);
    Node* r4 = __find_pc(&sets_ru_pc, x);

    if(r1) {
        fprintf(output_file, "%d ", r1->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    if(r2) {
        fprintf(output_file, "%d ", r2->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    if(r3) {
        fprintf(output_file, "%d ", r3->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    if(r4) {
        fprintf(output_file, "%d ", r4->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    fprintf(output_file, "\n");
}

void union_set(int x, int y) {
    Node* r1 = __union(&sets_none, x, y, __find);
    Node* r2 = __union_ru(&sets_ru, x, y, __find);
    Node* r3 = __union(&sets_pc, x, y, __find_pc);
    Node* r4 = __union_ru(&sets_ru_pc, x, y, __find_pc);

    if(r1) {
        fprintf(output_file, "%d ", r1->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }

    if(r2) {
        fprintf(output_file, "%d ", r2->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }

    if(r3) {
        fprintf(output_file, "%d ", r3->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }

    if(r4) {
        fprintf(output_file, "%d ", r4->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }

    fprintf(output_file, "\n");
}

void count_access() {
   fprintf(output_file,
           "%d %d %d %d\n",
           sets_none.count_access,
           sets_ru.count_access,
           sets_pc.count_access,
           sets_ru_pc.count_access);
}

bool __makeset(Sets *set, int x) {
    if(set->nodes[x] != NULL)
        return false;

    Node* node = malloc(sizeof(Node));
    node->x = x;
    node->parent = node;
    node->rank = 0;
    set->nodes[x] = node;
    return true;
}

Node* __find(Sets *set, int x) {
    Node *n = set->nodes[x];
    if(n == NULL) 
        return NULL;

    set->count_access += 1;
    while(n->parent != n) {
        n = n->parent;
        set->count_access += 1;
    }

    return n;
}

Node* __find_pc_aux(Node* n, int* count_access) {
   *count_access += 1; 
   if(n->parent == n) return n;
   return n->parent = __find_pc_aux(n->parent, count_access);
}

Node* __find_pc(Sets *set, int x) {
    Node *n = set->nodes[x];
    return __find_pc_aux(n, &set->count_access);
}

Node* __union(Sets *sets, int x, int y, FindFn find_fn) {
    Node *x_ptr = sets->nodes[x];
    Node *y_ptr = sets->nodes[y];

    if(x_ptr == NULL || y_ptr == NULL)
        return NULL;

    Node* x_root_ptr = find_fn(sets, x);
    Node* y_root_ptr = find_fn(sets, y);

    if(x_root_ptr != y_root_ptr) {
        y_root_ptr->parent = x_root_ptr;
    }

    return x_root_ptr;
}

Node* __union_ru(Sets *sets, int x, int y, FindFn find_fn) {
    Node *x_ptr = sets->nodes[x];
    Node *y_ptr = sets->nodes[y];

    if(x_ptr == NULL || y_ptr == NULL)
        return NULL;

    Node* x_root_ptr = find_fn(sets, x);
    Node* y_root_ptr = find_fn(sets, y);

    if(x_root_ptr == y_root_ptr) {
        return x_root_ptr;
    }

    if(x_root_ptr->rank == y_root_ptr->rank) {
        y_root_ptr->parent = x_root_ptr;
        x_root_ptr->rank += 1; 
        return x_root_ptr;
    }

    Node *small_ptr, *large_ptr;
    if(x_root_ptr->rank > y_root_ptr->rank) {
        small_ptr = y_root_ptr;
        large_ptr = x_root_ptr;
    } else {
        small_ptr = x_root_ptr;
        large_ptr = y_root_ptr;
    }

    small_ptr->parent = large_ptr;
    return large_ptr;
}
