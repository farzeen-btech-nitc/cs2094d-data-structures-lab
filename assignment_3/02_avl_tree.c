#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* AVL Tree */

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
    int height;
};
typedef struct Node Node;

struct SearchResult {
    Node *node;
    Node *parent;
};
typedef struct SearchResult SearchResult;

Node *node_create();
void free_tree(Node *root);

Node *insert(Node *root, int data);
Node *delete (Node *root, int data);
SearchResult search(Node *root, int key);
Node *rotate_left(Node *x);
Node *rotate_right(Node *x);
int get_balance(Node *x);
void print_tree(FILE *fp, const Node *root);
int is_avl(Node *root);

FILE *INFILE;
FILE *OUTFILE;

int main() {

    // INFILE = fopen("input.txt", "r");
    // OUTFILE = fopen("output.txt", "w");
    INFILE = stdin;
    OUTFILE = stdout;

    char BUF[1024];
    Node *root = NULL;
    while (fgets(BUF, 1024, INFILE) != NULL) {
        int x;
        SearchResult s;
        switch (BUF[0]) {
        case '1':
            x = atoi(BUF + 1);
            root = insert(root, x);
            break;

        case '2':
            x = atoi(BUF + 1);
            root = delete (root, x);
            break;

        case '3':
            x = atoi(BUF + 1);
            s = search(root, x);
            if (s.node == NULL) {
                fprintf(OUTFILE, "NOT FOUND\n");
                break;
            }
            if (s.parent != NULL) {
                if (s.node == s.parent->left)
                    s.parent->left = rotate_left(s.node);
                else
                    s.parent->right = rotate_left(s.node);
            } else {
                root = rotate_left(s.node);
            }
            break;

        case '4':
            x = atoi(BUF + 1);
            s = search(root, x);
            if (s.node == NULL) {
                fprintf(OUTFILE, "NOT FOUND\n");
                break;
            }
            if (s.parent != NULL) {
                if (s.node == s.parent->left)
                    s.parent->left = rotate_right(s.node);
                else
                    s.parent->right = rotate_right(s.node);
            } else {
                root = rotate_right(s.node);
            }
            break;

        case '5':
            x = atoi(BUF + 1);
            s = search(root, x);
            if (s.node == NULL) {
                fprintf(OUTFILE, "NOT FOUND\n");
                break;
            }

            fprintf(OUTFILE, "%d\n", get_balance(s.node));
            break;

        case '6':
            print_tree(OUTFILE, root);
            break;

        case '7':
            fprintf(OUTFILE, is_avl(root) ? "TRUE\n" : "FALSE\n");
            break;

        case '8':
            x = atoi(BUF + 1);
            s = search(root, x);
            if (s.node == NULL) {
                fprintf(OUTFILE, "FALSE\n");
            } else {
                fprintf(OUTFILE, "TRUE\n");
            }
            break;

        case '9':
            return 0;

        default:
            fprintf(OUTFILE, "Unexpected input: \n");
            fprintf(OUTFILE, "'%s'\n", BUF);
            return -1;
        }
    }

    fclose(INFILE);
    fclose(OUTFILE);

    return 0;
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))

Node *rotate_left(Node *x) {
    /*
         x                  y
        / \                / \
      .a   y     ===>     x  .z
          / \            / \
         b  .z         .a   b
    */
    Node *y = x->right;
    Node *b = y->left;
    x->right = b;
    y->left = x;

    int h_left = x->left != NULL ? x->left->height : 0;
    int h_right = x->right != NULL ? x->right->height : 0;
    x->height = MAX(h_left, h_right) + 1;

    h_left = y->left != NULL ? y->left->height : 0;
    h_right = y->right != NULL ? y->right->height : 0;
    y->height = MAX(h_left, h_right) + 1;

    return y;
}

Node *rotate_right(Node *x) {
    /*
           x              y
          / \            / \
         y  .a  ===>   .z   x
        / \                / \
      .z   b              b  .a
    */
    Node *y = x->left;
    Node *b = y->right;
    x->left = b;
    y->right = x;

    int h_left = x->left != NULL ? x->left->height : 0;
    int h_right = x->right != NULL ? x->right->height : 0;
    x->height = MAX(h_left, h_right) + 1;

    h_left = y->left != NULL ? y->left->height : 0;
    h_right = y->right != NULL ? y->right->height : 0;
    y->height = MAX(h_left, h_right) + 1;

    return y;
}


Node *node_create() {
    Node *result = malloc(sizeof(Node));
    result->left = NULL;
    result->right = NULL;
    result->data = INT_MAX;
    result->height = 1;
    return result;
}

int get_balance(Node *node) {
    int h_left = node->left != NULL ? node->left->height : 0;
    int h_right = node->right != NULL ? node->right->height : 0;
    int balance = h_right - h_left;
    return balance;
}

Node *insert(Node *node, int data) {
    if (node == NULL) {
        node = node_create();
        node->data = data;
        return node;
    }

    if (data < node->data) {
        node->left = insert(node->left, data);
    } else if (data > node->data) {
        node->right = insert(node->right, data);
    } else {
        return node;
    }

    int h_left = node->left != NULL ? node->left->height : 0;
    int h_right = node->right != NULL ? node->right->height : 0;
    node->height = MAX(h_left, h_right) + 1;

    int balance = h_right - h_left;

    if (balance > 1 && data > node->right->data) {
        return rotate_left(node);
    }
    if (balance < -1 && data < node->left->data) {
        return rotate_right(node);
    }
    if (balance > 1 && data < node->right->data) {
        node->right = rotate_right(node->right);
        return rotate_left(node);
    }
    if (balance < -1 && data > node->left->data) {
        node->left = rotate_left(node->left);
        return rotate_right(node);
    }

    return node;
}


SearchResult search(Node *root, int elem) {
    if (root == NULL) {
        SearchResult ret = {NULL, NULL};
        return ret;
    }

    Node *cur = root;
    Node *parent = NULL;
    while (cur != NULL) {
        if (elem == cur->data) {
            SearchResult ret = {cur, parent};
            return ret;
        }
        parent = cur;
        if (elem < cur->data) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    SearchResult ret = {NULL, NULL};
    return ret;
}

Node *tree_min(Node *root) {
    Node *cur = root;
    while (cur->left != NULL)
        cur = cur->left;
    return cur;
}

Node *delete (Node *node, int data) {
    if (node == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return node;
    }

    if (data < node->data) {
        node->left = delete (node->left, data);
    } else if (data > node->data) {
        node->right = delete (node->right, data);
    } else {
        if (node->left == NULL && node->right == NULL) {
            free(node);
            node = NULL;
        } else if (node->left == NULL || node->right == NULL) {
            Node *child = node->left != NULL ? node->left : node->right;
            node->data = child->data;
            node->left = child->left;
            node->right = child->right;
            node->height = child->height;
        } else {
            Node *succ = tree_min(node->right);
            node->data = succ->data;
            node->right = delete (node->right, succ->data);
        }
    }
    if (node == NULL)
        return node;

    int h_left = node->left != NULL ? node->left->height : 0;
    int h_right = node->right != NULL ? node->right->height : 0;
    node->height = MAX(h_left, h_right) + 1;
    int balance = h_right - h_left;

    if (balance > 1 && get_balance(node->right) < 0) {
        return rotate_left(node);
    }
    if (balance > 1 && get_balance(node->right) >= 0) {
        node->left = rotate_left(node->left);
        return rotate_right(node);
    }
    if (balance < -1 && get_balance(node->left) > 0) {
        return rotate_left(node);
    }

    if (balance < -1 && get_balance(node->left) <= 0) {
        node->left = rotate_left(node->left);
        return rotate_left(node);
    }

    return node;
}

void free_tree(Node *root) {
    if (root == NULL)
        return;
    free_tree(root->left);
    free_tree(root->right);
    free(root);
}

void print_tree(FILE *fp, const Node *root) {
    fprintf(fp, "(");
    if (root != NULL && root->data != INT_MAX) {
        fprintf(fp, "%d", root->data);
        if (root->left != NULL || root->right != NULL) {
            print_tree(fp, root->left);
            print_tree(fp, root->right);
        }
    }
    fprintf(fp, ")\n");
}

int is_avl(Node *root) {
    return root == NULL || (get_balance(root) >= -1 && get_balance(root) <= 1 &&
                            is_avl(root->left) && is_avl(root->right));
}