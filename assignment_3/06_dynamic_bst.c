#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct DynamicBS {
    int *array[32];
    int len;
};
typedef struct DynamicBS DynamicBS;

int bin_search(int *arr, int a, int b, int x);

void insert(DynamicBS *b, int x);
int search(DynamicBS *b, int x);
void print(DynamicBS *b);

FILE *output_file;
FILE *input_file;

int main() {
    output_file = stdout;
    input_file = stdin;

    char buf[1024];
    DynamicBS bs;
    for (int i = 0; i < 32; i++) {
        bs.array[i] = 0;
    }
    bs.len = 0;

    while (fgets(buf, 1024, input_file) != NULL) {
        int x;

        switch (buf[0]) {
        case 'i':
            x = atoi(buf + 1);
            insert(&bs, x);
            break;

        case 's':
            x = atoi(buf + 1);
            int idx = search(&bs, x);
            if (idx != -1) {
                fprintf(output_file, "%d\n", idx);
            } else {
                fprintf(output_file, "NIL\n");
            }
            break;

        case 'p':
            print(&bs);
            break;

        case 'x':
            return 0;

        default:
            fprintf(output_file, "Unexpected input: \n");
            fprintf(output_file, "'%s'\n", buf);
            return -1;
        }
    }
    return 0;
}

int bin_search(int *arr, int a, int b, int x) {
    if (a > b)
        return -1;
    int m = (a + b) / 2;
    if (arr[m] == x)
        return m;
    if (x < arr[m])
        return bin_search(arr, a, m - 1, x);
    if (x > arr[m])
        return bin_search(arr, m + 1, b, x);
    return -1;
}

void merge(int *a, int a_len, int *dest, int dest_len) {
    int *temp = malloc(sizeof(int) * (a_len + dest_len));

    int i = 0, j = 0, k = 0;
    while (i < a_len && j < dest_len) {
        if (a[i] < dest[j]) {
            temp[k] = a[i];
            k++;
            i++;
        } else {
            temp[k] = dest[j];
            k++;
            j++;
        }
    }

    while (i < a_len) {
        temp[k] = a[i];
        k++;
        i++;
    }

    while (j < dest_len) {
        temp[k] = dest[j];
        k++;
        j++;
    }

    memcpy(dest, temp, sizeof(int) * k);
}

void insert(DynamicBS *b, int x) {
    int first_empty = 0;
    while (b->array[first_empty] != NULL)
        first_empty += 1;

    b->array[first_empty] = malloc(sizeof(int) * 1 << first_empty);
    int *dest = b->array[first_empty];
    int dest_len = 0;

    for (int i = 0; i < first_empty; i++) {
        merge(b->array[i], 1 << i, dest, dest_len);
        dest_len += 1 << i;
        free(b->array[i]);
        b->array[i] = NULL;
    }

    int a[1] = {x};
    merge(a, 1, dest, dest_len);
    dest_len++;
    assert(dest_len == 1 << first_empty);
}

void print(DynamicBS *b) {
    for (int i = 0; i < 32; i++) {
        if (b->array[i] == NULL)
            continue;

        int *array = b->array[i];
        for (int j = 0; j < 1 << i; j++) {
            fprintf(output_file, "%d ", array[j]);
        }
        fprintf(output_file, "\n");
    }
}

int search(DynamicBS *b, int x) {
    for (int i = 0; i < 32; i++) {
        if (b->array[i] == NULL)
            continue;

        int *array = b->array[i];
        int idx = bin_search(array, 0, (1 << i) - 1, x);
        if (idx != -1)
            return idx;
    }

    return -1;
}