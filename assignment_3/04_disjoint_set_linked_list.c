#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node;
struct LinkedList;

struct LinkedList {
    struct Node *head;
    struct Node *tail;
    int length;
};
typedef struct LinkedList LinkedList;

struct Node {
    int x;
    struct LinkedList *parent;
    struct Node *next;
};
typedef struct Node Node;

void makeset(int v);
void find(int x);
void union_set(int x, int y);

FILE *output_file;
FILE *input_file;

#define bool int
#define true 1
#define false 0

int main() {
    output_file = stdout;
    input_file = stdin;

    char buf[1024];
    while (fgets(buf, 1024, input_file) != NULL) {
        int x, y;
        char *x_str, *y_str;
        switch (buf[0]) {
        case 'm':
            x = atoi(buf + 1);
            makeset(x);
            break;

        case 'f':
            x = atoi(buf + 1);
            find(x);
            break;

        case 'u':
            strtok(buf, " ");
            x_str = strtok(NULL, " ");
            y_str = strtok(NULL, " ");
            x = atoi(x_str);
            y = atoi(y_str);
            union_set(x, y);
            break;

        case 's':
            return 0;

        default:
            fprintf(output_file, "Unexpected input: \n");
            fprintf(output_file, "'%s'\n", buf);
            return -1;
        }
    }
    return 0;
}

struct Sets {
    Node *nodes[10000];
};
typedef struct Sets Sets;

Sets sets, sets_simple;

// returns false if already exists
bool _makeset(Sets *sets, int x);
LinkedList *_find(Sets *sets, int x);

LinkedList *_union(Sets *sets, int x, int y);
LinkedList *_union_simple(Sets *sets, int x, int y);


void makeset(int x) {
    bool m1 = _makeset(&sets, x);
    if (!m1) {
        fprintf(output_file, "PRESENT\n");
    } else {
        fprintf(output_file, "%d\n", x);
    }
    _makeset(&sets_simple, x);
}

void find(int x) {

    Node *r2 = _find(&sets_simple, x)->head;
    if (r2) {
        fprintf(output_file, "%d ", r2->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    Node *r1 = _find(&sets, x)->head;
    if (r1) {
        fprintf(output_file, "%d ", r1->x);
    } else {
        fprintf(output_file, "NOT FOUND\n");
    }

    fprintf(output_file, "\n");
}

void union_set(int x, int y) {
    Node *r2 = _union_simple(&sets_simple, x, y)->head;
    if (r2) {
        fprintf(output_file, "%d ", r2->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }

    Node *r1 = _union(&sets, x, y)->head;
    if (r1) {
        fprintf(output_file, "%d ", r1->x);
    } else {
        fprintf(output_file, "ERROR\n");
    }
    fprintf(output_file, "\n");
}

bool _makeset(Sets *set, int x) {
    if (set->nodes[x] != NULL)
        return false;

    Node *node = malloc(sizeof(Node));
    node->x = x;
    node->next = NULL;

    LinkedList *list = malloc(sizeof(LinkedList));
    list->head = list->tail = node;
    list->length = 1;

    node->parent = list;

    set->nodes[x] = node;

    return true;
}

LinkedList *_find(Sets *set, int x) {
    Node *n = set->nodes[x];
    if (n == NULL)
        return NULL;
    return n->parent;
}

LinkedList *_union(Sets *sets, int x, int y) {
    Node *x_ptr = sets->nodes[x];
    Node *y_ptr = sets->nodes[y];

    if (x_ptr == NULL || y_ptr == NULL)
        return NULL;

    LinkedList *x_list = _find(sets, x);
    LinkedList *y_list = _find(sets, y);

    if (x_list == y_list) {
        return x_list;
    }

    if (x_list->length >= y_list->length) {
        for (Node *n = y_list->head; n != NULL; n = n->next) {
            n->parent = x_list;
        }

        x_list->tail->next = y_list->head;
        x_list->tail = y_list->tail;
        x_list->length += y_list->length;
        free(y_list);
        return x_list;
    } else {
        for (Node *n = x_list->head; n != NULL; n = n->next) {
            n->parent = y_list;
        }

        y_list->tail->next = x_list->head;
        y_list->tail = x_list->tail;
        y_list->length += x_list->length;
        free(x_list);
        return y_list;
    }
}

LinkedList *_union_simple(Sets *sets, int x, int y) {
    Node *x_ptr = sets->nodes[x];
    Node *y_ptr = sets->nodes[y];

    if (x_ptr == NULL || y_ptr == NULL)
        return NULL;

    LinkedList *x_list = _find(sets, x);
    LinkedList *y_list = _find(sets, y);

    if (x_list == y_list) {
        return x_list;
    }

    for (Node *n = y_list->head; n != NULL; n = n->next) {
        n->parent = x_list;
    }

    x_list->tail->next = y_list->head;
    x_list->tail = y_list->tail;
    x_list->length += y_list->length;
    free(y_list);
    return x_list;
}