#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>

struct Node {
    char m;
    int freq;
    struct Node* left;
    struct Node* right;
    struct Node* parent;
};
typedef struct Node Node;

struct Heap {
    Node** ptr_array;
    int len;
};
typedef struct Heap Heap;


Node* Find_Huffman_Code(char *msg);
void  Print_Huffman_Code(char *msg, Node* huffman_tree);

Heap  heap_build(Node** ptr_array, int len);
void  heap_min_heapify(Heap* heap, int start);
Node* heap_extract_min(Heap* heap);
void  heap_insert(Heap* heap, Node* value);

FILE* infile;
FILE* outfile;

int main() {
    // #define DEBUG
    #ifdef DEBUG
    infile = stdin;
    outfile = stdout;
    #else
    infile = fopen("input.txt", "r");
    outfile = fopen("output.txt", "w");
    #endif


    char buf[1024];
    while(fgets(buf, 1024, infile) != NULL) {
        int i = 0;
        while(buf[i]!='\0') i++;
        if(buf[i-1] == '\n') buf[i-1] = '\0';
        if(i == 1) break;
        
        Node* code = Find_Huffman_Code(buf);
        Print_Huffman_Code(buf, code);
    }
}

#define HEAP_LEFT(i) ((2*i)+1)
#define HEAP_RIGHT(i) ((2*i)+2)
#define HEAP_PARENT(i) ((i-1)/2)

void heap_min_heapify(Heap* heap, int start) {
    Node** ptr_array = heap->ptr_array;
    int len = heap->len;

    if(start>=len) return;

    int left = HEAP_LEFT(start);
    int right = HEAP_RIGHT(start);

    int min_index = -1;
    int min_val = INT_MAX;

    if(left < len && ptr_array[left]->freq < min_val) {
        min_index = left;
        min_val = ptr_array[left]->freq;
    }

    if(right < len && ptr_array[right]->freq < min_val) {
        min_index = right;
        min_val = ptr_array[right]->freq;
    }

    if(ptr_array[start]->freq > min_val) {
        Node* temp = ptr_array[start];
        ptr_array[start] = ptr_array[min_index];
        ptr_array[min_index] = temp;
        heap_min_heapify(heap, min_index);
    }
}

Heap heap_build(Node** array, int len) {
    Heap heap = {array, len};
    for(int i = len/2; i>=0; i--) {
        heap_min_heapify(&heap, i);
    }
    return heap;
}

Node* heap_extract_min(Heap* heap) {
    assert(heap != NULL);
    assert(heap->len != 0);
    assert(heap->ptr_array != NULL);

    Node* result = heap->ptr_array[0];
    heap->ptr_array[0] = heap->ptr_array[heap->len - 1];
    heap->len -= 1;
    heap_min_heapify(heap, 0);
    return result;
}

void heap_insert(Heap* heap, Node* value) {
    heap->ptr_array[heap->len] = value;
    heap->len += 1;
    int i = heap->len - 1;
    while(i>0 && 
          (heap->ptr_array[i]->freq) < (heap->ptr_array[HEAP_PARENT(i)]->freq)) {
        Node* temp = heap->ptr_array[i];
        heap->ptr_array[i] = heap->ptr_array[HEAP_PARENT(i)];
        heap->ptr_array[HEAP_PARENT(i)] = temp;
        i = HEAP_PARENT(i);
    }
}

Heap build_letter_freq_heap(char *msg) {
    int freq_array[256];
    for(int i = 0; i<256; i++) {
        freq_array[i] = 0;
    }

    for(int i = 0; msg[i] != '\0'; i++) {
        freq_array[(int)msg[i]] += 1;
    }

    int len = 0;
    for(int i = 0; i<256; i++) {
        if(freq_array[i] > 0) len += 1;
    }

    Node** ptr_array = malloc(len*sizeof(Node *));

    int top = 0;
    for(int i = 0; i<256; i++) {
        if(freq_array[i] > 0) {
            Node* n = malloc(sizeof(Node));
            n->m = (char) i;
            n->freq = freq_array[i];
            n->left = n->right = n->parent = NULL;
            ptr_array[top] = n;
            top+=1;
        }
    }

    return heap_build(ptr_array, len);
}

Node* Find_Huffman_Code(char *msg) {
    Heap letter_heap = build_letter_freq_heap(msg);
    while(letter_heap.len > 1) {
        Node* n1 = heap_extract_min(&letter_heap);
        Node* n2 = heap_extract_min(&letter_heap);
        
        Node* meta = malloc(sizeof(Node));
        meta->m = '\0';
        meta->freq = n1->freq + n2->freq;
        meta->left = n1;
        meta->right = n2;
        n1->parent = n2->parent = meta;

        heap_insert(&letter_heap, meta);
    }

    Node *root = heap_extract_min(&letter_heap);
    assert(letter_heap.len == 0);
    return root;
}

void get_leaf_nodes(Node **leaf_nodes, Node* root) {
    if(root->left == NULL && root->right == NULL) {
        leaf_nodes[(int) root->m] = root;
    } else {
        get_leaf_nodes(leaf_nodes, root->left);
        get_leaf_nodes(leaf_nodes, root->right);
    }
}

void Print_Huffman_Code(char *msg, Node* huffman_tree) {
    Node* (leaf_nodes[256]);
    get_leaf_nodes(leaf_nodes, huffman_tree);

    for(int i = 0; msg[i]!='\0'; i++) {
        Node* leaf = leaf_nodes[(int) msg[i]];

        // Why not use a long int for code?
        // Can't differentiate 001, and 01


        char code[257];
        code[256] = '\0';
        int code_start = 256; // code len = 256-code_start;
        
        Node* cur = leaf;
        while(cur->parent != NULL) {
            code_start -= 1;
            code[code_start] = cur == cur->parent->right?'1':'0';
            cur = cur->parent;
        }

        char *code_str = code+code_start;
        fprintf(outfile, "%s ", code_str);
    }
}