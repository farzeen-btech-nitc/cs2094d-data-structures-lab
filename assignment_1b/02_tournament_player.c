#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Heap {
    int* array;
    int len;
};
typedef struct Heap Heap;

void str_to_int_array(char* s, int* a, int* len, int capacity);
Heap extend_and_build_min_heap(int* a, int* len, int capacity);
int  get_second_best_player(Heap* heap);

void tournament_tree(char* s);


FILE* infile;
FILE* outfile;

int main() {
    // #define DEBUG
    #ifdef DEBUG
    infile = stdin;
    outfile = stdout;
    #else
    infile = fopen("input.txt", "r");
    outfile = fopen("output.txt", "w");
    #endif


    char buf[1024];
    while(fgets(buf, 1024, infile) != NULL) {
        tournament_tree(buf);
    }

    #ifndef DEBUG
    fclose(infile);
    fclose(outfile);
    #endif
}


void tournament_tree(char* s) {
    int teams[1024];
    const int capacity = 1024;
    int len = 0;
    str_to_int_array(s, teams, &len, capacity);
    Heap h = extend_and_build_min_heap(teams, &len, capacity);
    int second_best_player = get_second_best_player(&h);
    fprintf(outfile, "%d\n", second_best_player);
}


void str_to_int_array(char* s, int *a, int* len, int capacity) {
    *len = 0;
    for(char* tok = strtok(s, " \n"); tok != NULL; tok = strtok(NULL, " \n")) {
        assert(*len < capacity);
        a[*len] = atoi(tok);
        *len += 1;
    }
}

#define HEAP_LEFT(i) ((2*i)+1)
#define HEAP_RIGHT(i) ((2*i)+2)
#define HEAP_PARENT(i) ((i-1)/2)

void __extend_heap(int *a, int start, int len) {
    int left = HEAP_LEFT(start);
    int right = HEAP_RIGHT(start);

    // If left child is not there, it is a leaf node.
    if(left>=len) return;

    __extend_heap(a, left, len);
    __extend_heap(a, right, len);

    a[start] = a[left] < a[right] ? a[left] : a[right];
}

Heap extend_and_build_min_heap(int *a, int* len, int capacity) {

    assert(*len + *len - 1 <= capacity);
    
    // make a[0..len-1] the leaves
    // there will be len-1 internal nodes
    for(int i = *len-1; i>=0; i--) {
        int j = *len-1 + i;
        a[j] = a[i];
    }
    *len = *len + *len - 1;

    __extend_heap(a, 0, *len);

    Heap heap = {a, *len};
    return heap;
}


int get_second_best_player(Heap* heap) {
    int previous_player;
    if(heap->array[0] == heap->array[HEAP_LEFT(0)]) {
        previous_player = HEAP_LEFT(0);
    } else {
        previous_player = HEAP_RIGHT(0);
    }

    int opponent;
    if(heap->array[previous_player] == heap->array[HEAP_LEFT(previous_player)]) {
        opponent = HEAP_RIGHT(previous_player);
    } else {
        opponent = HEAP_LEFT(previous_player);
    }

    return heap->array[opponent];
}