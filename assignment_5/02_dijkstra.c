#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Graph {
    int len;
    int **adj;
    int **weight;
};
typedef struct Graph Graph;

struct Dijsktra {
    int *parent;
    int *distance;
};
typedef struct Dijsktra Dijsktra;

Graph read_graph();

Dijsktra dijkstra(const Graph *g, int s);
void print_all_distance(const Graph *g, const Dijsktra *d);

FILE *INFILE;
FILE *OUTFILE;

int main() {
    INFILE = fopen("input.txt", "r");
    OUTFILE = fopen("output.txt", "w");

    //printf("TO READ\n");
    Graph g = read_graph();
    //printf("LEN: %d\n", g.len);
    //printf("READ\n");
    char buf[1024];
    fgets(buf, 1024, INFILE);
    int s = atoi(buf);

    Dijsktra d = dijkstra(&g, s);
    print_all_distance(&g, &d);
}

Graph read_graph() {
    Graph G;
    char buf[1024];

    fgets(buf, 1024, INFILE);
    int V = atoi(buf);
    G.len = V;

    fgets(buf, 1024, INFILE);
    int E = atoi(buf);

    G.adj = malloc(sizeof(int *) * V);
    G.weight = malloc(sizeof(int *) * V);

    for (int i = 0; i < V; i++) {
        G.adj[i] = malloc(sizeof(int) * V);
        G.adj[i][0] = 0; // LEN
        G.weight[i] = malloc(sizeof(int) * V);
    }

    for (int i = 0; i < E; i++) {
        fgets(buf, 1024, INFILE);
        int u = atoi(strtok(buf, " \n"));
        int v = atoi(strtok(NULL, " \n"));
        int w = atoi(strtok(NULL, " \n"));

        int adj_len = G.adj[u][0];
        G.adj[u][adj_len + 1] = v;
        adj_len += 1;
        G.adj[u][0] = adj_len;

        G.weight[u][v] = w;
    }

    // for (int i = 0; i < V; i++) {
    //     printf("[%d] => ", i);
    //     for (int j = 1; j <= G.adj[i][0]; j++) {
    //         printf("%d ", G.adj[i][j]);
    //     }
    //     printf("\n");
    // }

    return G;
}


Dijsktra initialize_single_source(const Graph *g, int s) {
    Dijsktra d;
    d.distance = malloc(sizeof(int) * g->len);
    d.parent = malloc(sizeof(int) * g->len);
    for (int i = 0; i < g->len; i++) {
        d.distance[i] = INT_MAX;
    }

    d.distance[s] = 0;
    return d;
}

void relax(const Graph *g, Dijsktra *d, int u, int v) {
    if (d->distance[u] == INT_MAX) {
        //printf("!! u=%d, v=%d\n", u, v);
    } else {
        //printf("   u=%d, v=%d\n", u, v);
    }
    if (d->distance[u] != INT_MAX &&
        d->distance[v] > d->distance[u] + g->weight[u][v]) {
        d->distance[v] = d->distance[u] + g->weight[u][v];
        d->parent[v] = u;
    }
    //printf("[%d, %d] => %d\n", u, v, d->distance[v]);
}

void swap(int *array, int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

#define LEFT(i) (2 * (i) + 1)
#define RIGHT(i) (2 * (i) + 2)
#define PARENT(i) (((i)-1) / 2)

// fix the minheap rooted at root, assuming the minheaps rooted
// at its children are correct
void heap_bubble_down(int *idx, const int *keys, int root, int end) {
    int smaller_child;
    if (RIGHT(root) <= end) {
        if (keys[idx[LEFT(root)]] < keys[idx[RIGHT(root)]]) {
            smaller_child = LEFT(root);
        } else {
            smaller_child = RIGHT(root);
        }
    } else if (LEFT(root) <= end) {
        smaller_child = LEFT(root);
    } else {
        return;
    }
    if (keys[idx[root]] > keys[idx[smaller_child]]) {
        swap(idx, root, smaller_child);
        heap_bubble_down(idx, keys, smaller_child, end);
    }
}

void heap_min_heapify(int *idx, const int *keys, int len) {
    int start = PARENT(len - 1);
    while (start >= 0) {
        heap_bubble_down(idx, keys, start, len - 1);
        start--;
    }
}

int heap_extract_min(int *idx, const int *keys, int *len) {
    int ret = idx[0];
    idx[0] = idx[*len - 1];
    *len = *len - 1;
    heap_bubble_down(idx, keys, 0, *len - 1);
    return ret;
}

void heap_bubble_up(int *idx, const int *keys, int x) {
    while (PARENT(x) >= 0) {
        if (keys[idx[x]] < keys[idx[PARENT(x)]]) {
            swap(idx, x, PARENT(x));
            x = PARENT(x);
        } else {
            break;
        }
    }
}


Dijsktra dijkstra(const Graph *g, int s) {
    Dijsktra d = initialize_single_source(g, s);
    int *idx_heap = malloc(sizeof(int) * g->len);
    int idx_heap_len = g->len;
    for (int i = 0; i < idx_heap_len; i++)
        idx_heap[i] = i;
    heap_min_heapify(idx_heap, d.distance, idx_heap_len);

    while (idx_heap_len > 0) {
        int u = heap_extract_min(idx_heap, d.distance, &idx_heap_len);
        //printf(">> %d\n", u);
        int u_adj_len = g->adj[u][0];
        for (int i = 1; i <= u_adj_len; i++) {
            int v = g->adj[u][i];
            relax(g, &d, u, v);
            for (int j = 0; j < idx_heap_len; j++) {
                if (idx_heap[j] == v) {
                    heap_bubble_up(idx_heap, d.distance, j);
                    break;
                }
            }
        }
    }
    return d;
}

void print_all_distance(const Graph *g, const Dijsktra *d) {
    // printf("ALL DIST\n");
    for (int i = 0; i < g->len; i++) {
        fprintf(OUTFILE, "%d %d\n", i, d->distance[i]);
    }
    // printf("END ALL DIST\n");
}
