#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define bool int
#define true 1
#define false 0

FILE *INFILE;
FILE *OUTFILE;

struct Graph {
    int len;
    int **adj;
    int **weight;
};
typedef struct Graph Graph;

struct DSNode {
    int x;
    struct DSNode *parent;
    int rank;
};
typedef struct DSNode DSNode;

struct DSets {
    DSNode *nodes[10000];
};
typedef struct DSets DSets;


void dsets_makeset(DSets *sets, int x);
DSNode *dsets_find(DSets *sets, int x);
DSNode *dsets_union(DSets *sets, int x, int y);

Graph read_graph();
int kruskal_sum_weight(const Graph *g);


int main() {
    INFILE = fopen("input.txt", "r");
    OUTFILE = fopen("output.txt", "w");

    Graph g = read_graph();
    int w = kruskal_sum_weight(&g);
    fprintf(OUTFILE, "%d\n", w);
}

typedef int Edge[3];

int comp_edge(const void *a, const void *b) {
    const Edge *e1 = a;
    const Edge *e2 = b;
    return (*e1)[2] - (*e2)[2];
}

int kruskal_sum_weight(const Graph *g) {

    DSets dsets;
    for (int i = 0; i < 10000; i++) {
        dsets.nodes[i] = NULL;
    }

    for (int i = 0; i < g->len; i++) {
        dsets_makeset(&dsets, i);
    }

    // Calculate total number of edges
    int E = 0;
    for (int i = 0; i < g->len; i++)
        E += g->adj[i][0];

    Edge *edges = malloc(sizeof(Edge) * E);

    int E_ = 0;
    for (int i = 0; i < g->len; i++) {
        for (int j = 1; j <= g->adj[i][0]; j++) {
            int u = i;
            int v = g->adj[i][j];
            int w = g->weight[u][v];
            edges[E_][0] = u;
            edges[E_][1] = v;
            edges[E_][2] = w;
            E_++;
        }
    }

    qsort(edges, E, sizeof(Edge), comp_edge);

    int W = 0;
    for (int i = 0; i < E; i++) {
        int u = edges[i][0], v = edges[i][1], w = edges[i][2];
        if (dsets_find(&dsets, u) != dsets_find(&dsets, v)) {
            W += w;
            dsets_union(&dsets, u, v);
        }
    }

    return W;
}


Graph read_graph() {
    Graph G;
    char buf[1024];

    fgets(buf, 1024, INFILE);
    int N = atoi(buf);
    G.len = N;

    G.adj = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.adj[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);

        int j = 1;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            G.adj[i][j] = atoi(tok);
        }

        G.adj[i][0] = j - 1; // ARRAY LEN
    }

    G.weight = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.weight[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);
        int j = 1;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            int v = G.adj[i][j];
            G.weight[i][v] = atoi(tok);
        }
    }

    return G;
}


void dsets_makeset(DSets *set, int x) {
    if (set->nodes[x] != NULL)
        return;

    DSNode *node = malloc(sizeof(DSNode));
    node->x = x;
    node->parent = node;
    node->rank = 0;
    set->nodes[x] = node;
}

DSNode *dsets_find(DSets *sets, int x) {
    DSNode *n = sets->nodes[x];
    if (n == NULL)
        return NULL;

    while (n->parent != n) {
        n = n->parent;
    }

    return n;
}

DSNode *dsets_union(DSets *sets, int x, int y) {
    DSNode *x_ptr = sets->nodes[x];
    DSNode *y_ptr = sets->nodes[y];

    if (x_ptr == NULL || y_ptr == NULL)
        return NULL;

    DSNode *x_root_ptr = dsets_find(sets, x);
    DSNode *y_root_ptr = dsets_find(sets, y);

    if (x_root_ptr != y_root_ptr) {
        y_root_ptr->parent = x_root_ptr;
    }

    return x_root_ptr;
}