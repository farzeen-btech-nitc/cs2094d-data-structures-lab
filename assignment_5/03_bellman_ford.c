#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Graph {
    int len;
    int **adj;
    int **weight;
};
typedef struct Graph Graph;

Graph read_graph();

int bellman_ford(const Graph *g);

FILE *INFILE;
FILE *OUTFILE;

int main() {
    INFILE = fopen("input.txt", "r");
    OUTFILE = fopen("output.txt", "w");

    Graph g = read_graph();
    int no_neg_cycle = bellman_ford(&g);
    if (no_neg_cycle) {
        fprintf(OUTFILE, "-1\n");
    } else {
        fprintf(OUTFILE, "1\n");
    }
}

Graph read_graph() {
    Graph G;
    char buf[1024];

    fgets(buf, 1024, INFILE);
    int V = atoi(strtok(buf, " \n"));
    G.len = V;
    int E = atoi(strtok(NULL, " \n"));

    G.adj = malloc(sizeof(int *) * V);
    G.weight = malloc(sizeof(int *) * V);

    for (int i = 0; i < V; i++) {
        G.adj[i] = malloc(sizeof(int) * V);
        G.adj[i][0] = 0; // LEN
        G.weight[i] = malloc(sizeof(int) * V);
    }

    for (int i = 0; i < E; i++) {
        fgets(buf, 1024, INFILE);
        int u = atoi(strtok(buf, " \n"));
        int v = atoi(strtok(NULL, " \n"));
        int w = atoi(strtok(NULL, " \n"));

        int adj_len = G.adj[u][0];
        G.adj[u][adj_len + 1] = v;
        adj_len += 1;
        G.adj[u][0] = adj_len;

        G.weight[u][v] = w;
    }

    // for (int i = 0; i < V; i++) {
    //     printf("[%d] => ", i);
    //     for (int j = 1; j <= G.adj[i][0]; j++) {
    //         printf("%d ", G.adj[i][j]);
    //     }
    //     printf("\n");
    // }

    return G;
}

struct BellmanFord {
    int *distance
};
typedef struct BellmanFord BellmanFord;

BellmanFord initialize_single_source(const Graph *g, int s) {
    BellmanFord d;
    d.distance = malloc(sizeof(int) * g->len);
    for (int i = 0; i < g->len; i++) {
        d.distance[i] = INT_MAX;
    }

    d.distance[s] = 0;
    return d;
}

void relax(const Graph *g, BellmanFord *d, int u, int v) {
    if (d->distance[u] != INT_MAX &&
        d->distance[v] > d->distance[u] + g->weight[u][v]) {
        d->distance[v] = d->distance[u] + g->weight[u][v];
    }
}

typedef int Edge[2];

int bellman_ford(const Graph *g) {
    BellmanFord d = initialize_single_source(g, 0);

    // Calculate total number of edges
    int E = 0;
    for (int i = 0; i < g->len; i++)
        E += g->adj[i][0];

    Edge *edges = malloc(sizeof(Edge) * E);

    int E_ = 0;
    for (int i = 0; i < g->len; i++) {
        for (int j = 1; j <= g->adj[i][0]; j++) {
            int u = i;
            int v = g->adj[i][j];
            edges[E_][0] = u;
            edges[E_][1] = v;
            E_++;
        }
    }

    for (int i = 0; i < g->len - 1; i++) {
        for (int j = 0; j < E; j++) {
            relax(g, &d, edges[j][0], edges[j][1]);
        }
    }

    for (int j = 0; j < E; j++) {
        int u = edges[j][0], v = edges[j][1];
        if (d.distance[u] != INT_MAX &&
            d.distance[v] > d.distance[u] + g->weight[u][v]) {
            return 0;
        }
    }

    return 1;
}