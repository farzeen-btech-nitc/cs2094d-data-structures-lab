#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *INFILE;
FILE *OUTFILE;

struct Graph {
    int len;
    int **adj;
    int **weight;
};
typedef struct Graph Graph;

Graph read_graph();
int prim_sum_weight(const Graph *g);

int main() {
    INFILE = fopen("input.txt", "r");
    OUTFILE = fopen("output.txt", "w");

    Graph g = read_graph();
    int w = prim_sum_weight(&g);
    fprintf(OUTFILE, "%d\n", w);
}


Graph read_graph() {
    Graph G;
    char buf[1024];

    fgets(buf, 1024, INFILE);
    int N = atoi(buf);
    G.len = N;

    G.adj = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.adj[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);

        int j = 1;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            G.adj[i][j] = atoi(tok);
        }

        G.adj[i][0] = j - 1; // ARRAY LEN
    }

    G.weight = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.weight[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);
        int j = 1;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            int v = G.adj[i][j];
            G.weight[i][v] = atoi(tok);
        }
    }

    return G;
}


/* ---------------- HEAP ------------------------ */

void swap(int *array, int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

#define LEFT(i) (2 * (i) + 1)
#define RIGHT(i) (2 * (i) + 2)
#define PARENT(i) (((i)-1) / 2)

// fix the minheap rooted at root, assuming the minheaps rooted
// at its children are correct
void heap_bubble_down(int *idx, const int *keys, int root, int end) {
    int smaller_child;
    if (RIGHT(root) <= end) {
        if (keys[idx[LEFT(root)]] < keys[idx[RIGHT(root)]]) {
            smaller_child = LEFT(root);
        } else {
            smaller_child = RIGHT(root);
        }
    } else if (LEFT(root) <= end) {
        smaller_child = LEFT(root);
    } else {
        return;
    }
    if (keys[idx[root]] > keys[idx[smaller_child]]) {
        swap(idx, root, smaller_child);
        heap_bubble_down(idx, keys, smaller_child, end);
    }
}

void heap_min_heapify(int *idx, const int *keys, int len) {
    int start = PARENT(len - 1);
    while (start >= 0) {
        heap_bubble_down(idx, keys, start, len - 1);
        start--;
    }
}

int heap_extract_min(int *idx, const int *keys, int *len) {
    int ret = idx[0];
    idx[0] = idx[*len - 1];
    *len = *len - 1;
    heap_bubble_down(idx, keys, 0, *len - 1);
    return ret;
}

void heap_bubble_up(int *idx, const int *keys, int x) {
    while (PARENT(x) >= 0) {
        if (keys[idx[x]] < keys[idx[PARENT(x)]]) {
            swap(idx, x, PARENT(x));
            x = PARENT(x);
        } else {
            break;
        }
    }
}

int prim_sum_weight(const Graph *g) {
    int heap_len = g->len;
    int *heap_idx = malloc(sizeof(int) * heap_len);
    int *heap_keys = malloc(sizeof(int) * heap_len);
    for (int i = 0; i < heap_len; i++) {
        heap_idx[i] = i;
        heap_keys[i] = INT_MAX;
    }

    heap_keys[0] = 0;

    heap_min_heapify(heap_idx, heap_keys, heap_len);

    int W = 0;
    while (heap_len > 0) {
        int u = heap_extract_min(heap_idx, heap_keys, &heap_len);

        int u_adj_len = g->adj[u][0];
        for (int i = 1; i <= u_adj_len; i++) {
            int v = g->adj[u][i];
            int found = -1;
            for (int k = 0; k < heap_len; k++) {
                if (heap_idx[k] == v) {
                    found = k;
                    break;
                }
            }

            if (found != -1 && g->weight[u][v] < heap_keys[v]) {
                heap_keys[v] = g->weight[u][v];
                heap_bubble_up(heap_idx, heap_keys, found);
            }
        }

        W += heap_keys[u];
    }

    return W;
}