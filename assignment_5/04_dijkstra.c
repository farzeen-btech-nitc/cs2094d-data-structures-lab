#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Graph {
    int len;
    int **adj;
    int **weight;
};
typedef struct Graph Graph;

struct Dijsktra {
    int *parent;
    int *distance;
};
typedef struct Dijsktra Dijsktra;

Graph read_graph();

Dijsktra dijkstra(const Graph *g, int s);

void print_all_distance(const Graph *g, const Dijsktra *d);
void print_distance_to_t(const Dijsktra *d, int t);

FILE *INFILE;
FILE *OUTFILE;

int main() {
    INFILE = fopen("input.txt", "r");
    OUTFILE = fopen("output.txt" ,"w");

    //printf("TO READ\n");
    Graph g = read_graph();
    //printf("LEN: %d\n", g.len);
    //printf("READ\n");
    char buf[1024];
    while (fgets(buf, 1024, INFILE)) {
        char *tok = strtok(buf, " \n");
        if (strcmp(tok, "apsp") == 0) {
            int s = atoi(strtok(NULL, " \n"));
            Dijsktra d = dijkstra(&g, s);
            //printf("apsp\n");
            print_all_distance(&g, &d);
        } else if (strcmp(tok, "sssp") == 0) {
            int s = atoi(strtok(NULL, " \n"));
            int t = atoi(strtok(NULL, " \n"));

            Dijsktra d = dijkstra(&g, s);
            //printf("sssp\n");
            print_distance_to_t(&d, t);

        } else if (strcmp(tok, "stop") == 0) {
            return 0;
        } else {
            //printf("Invalid input: '%s'\n", tok);
        }
    }
}

Graph read_graph() {
    Graph G;
    char buf[1024];

    fgets(buf, 1024, INFILE);
    int N = atoi(buf);
    G.len = N;

    G.adj = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.adj[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);

        int j = 0;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            G.adj[i][j] = atoi(tok);
        }

        G.adj[i][j] = -1; // ARRAY END SENTINEL
    }

    G.weight = malloc(sizeof(int *) * N);

    for (int i = 0; i < N; i++) {
        G.weight[i] = malloc(sizeof(int) * N);
        fgets(buf, 1024, INFILE);
        int j = 0;
        char *tok = strtok(buf, " \n");
        for (; tok; (tok = strtok(NULL, " \n")), j++) {
            int v = G.adj[i][j];
            G.weight[i][v] = atoi(tok);
        }
    }

    return G;
}


Dijsktra initialize_single_source(const Graph *g, int s) {
    Dijsktra d;
    d.distance = malloc(sizeof(int) * g->len);
    d.parent = malloc(sizeof(int) * g->len);
    for (int i = 0; i < g->len; i++) {
        d.distance[i] = INT_MAX;
    }

    d.distance[s] = 0;
    return d;
}

void relax(const Graph *g, Dijsktra *d, int u, int v) {
    if (d->distance[u] == INT_MAX) {
        //printf("!! u=%d, v=%d\n", u, v);
    } else {
        //printf("   u=%d, v=%d\n", u, v);
    }
    if (d->distance[u] != INT_MAX &&
        d->distance[v] > d->distance[u] + g->weight[u][v]) {
        d->distance[v] = d->distance[u] + g->weight[u][v];
        d->parent[v] = u;
    }
    //printf("[%d, %d] => %d\n", u, v, d->distance[v]);
}

void swap(int *array, int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

#define LEFT(i) (2 * (i) + 1)
#define RIGHT(i) (2 * (i) + 2)
#define PARENT(i) (((i)-1) / 2)

// fix the minheap rooted at root, assuming the minheaps rooted
// at its children are correct
void heap_bubble_down(int *idx, const int *keys, int root, int end) {
    int smaller_child;
    if (RIGHT(root) <= end) {
        if (keys[idx[LEFT(root)]] < keys[idx[RIGHT(root)]]) {
            smaller_child = LEFT(root);
        } else {
            smaller_child = RIGHT(root);
        }
    } else if (LEFT(root) <= end) {
        smaller_child = LEFT(root);
    } else {
        return;
    }
    if (keys[idx[root]] > keys[idx[smaller_child]]) {
        swap(idx, root, smaller_child);
        heap_bubble_down(idx, keys, smaller_child, end);
    }
}

void heap_min_heapify(int *idx, const int *keys, int len) {
    int start = PARENT(len - 1);
    while (start >= 0) {
        heap_bubble_down(idx, keys, start, len - 1);
        start--;
    }
}

int heap_extract_min(int *idx, const int *keys, int *len) {
    int ret = idx[0];
    idx[0] = idx[*len - 1];
    *len = *len - 1;
    heap_bubble_down(idx, keys, 0, *len - 1);
    return ret;
}

void heap_bubble_up(int *idx, const int *keys, int x) {
    while (PARENT(x) >= 0) {
        if (keys[idx[x]] < keys[idx[PARENT(x)]]) {
            swap(idx, x, PARENT(x));
            x = PARENT(x);
        } else {
            break;
        }
    }
}


Dijsktra dijkstra(const Graph *g, int s) {
    Dijsktra d = initialize_single_source(g, s);
    int *idx_heap = malloc(sizeof(int) * g->len);
    int idx_heap_len = g->len;
    for (int i = 0; i < idx_heap_len; i++)
        idx_heap[i] = i;
    heap_min_heapify(idx_heap, d.distance, idx_heap_len);

    while (idx_heap_len > 0) {
        int u = heap_extract_min(idx_heap, d.distance, &idx_heap_len);
        for (int i = 0; g->adj[u][i] >= 0; i++) {
            int v = g->adj[u][i];
            relax(g, &d, u, v);
            for (int j = 0; j < idx_heap_len; j++) {
                if (idx_heap[j] == v) {
                    heap_bubble_up(idx_heap, d.distance, j);
                    break;
                }
            }
        }
    }
    return d;
}

void print_all_distance(const Graph *g, const Dijsktra *d) {
    //printf("ALL DIST\n");
    for (int i = 0; i < g->len; i++) {
        fprintf(OUTFILE, "%d ", d->distance[i]);
    }
    fprintf(OUTFILE, "\n");
    //printf("END ALL DIST\n");
}

void print_distance_to_t(const Dijsktra *d, int t) {
    fprintf(OUTFILE, "%d\n", d->distance[t]);
}