#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/* 01. Linked List, Highest Occurring */

struct Node {
    int data;
    int freq;
    struct Node *next;
};
typedef struct Node Node;

struct List {
    Node* head;
    Node* tail;
};
typedef struct List List;

Node* new_node(int data);

void  free_list_elements(List *list);  
/* list is considered stack allocated.
   Only elements are freed. */

List create(char *line_of_ints, int len);
void print(FILE *fp, const List *list);
int  h_occur(const List *list);
void add_node(List *list, int data);


int main() {
    #define _buf_len 1024
    char buf[_buf_len];
    FILE *infile = fopen("input.txt", "r");
    FILE *outfile = fopen("output.txt", "w");
    List list = {0, 0};

    int line_no = 0;
    while(fgets(buf, _buf_len, infile) != NULL) {
        fflush(outfile);
        // printf("%d: %s", line_no, buf);
        line_no ++;

        int n;
        switch(buf[0]) {
            case 'c':
                n = atoi(buf+1); 
                if(fgets(buf, _buf_len, infile) == NULL) {
                    printf("Unexpected EOF at line %d\n", line_no);
                    free_list_elements(&list);
                    return -1;
                }
                list = create(buf, n);
                break;
            case 'p':
                print(outfile, &list);
                break;
            case 'h':
                n = h_occur(&list);
                fprintf(outfile, "%d\n", n);
                break;
            case 'a':
                n = atoi(buf+1);
                add_node(&list, n);
                break;
            case 's':
                printf("Program terminated.\n");
                return 0;
        }
    }

    fclose(infile);
    fclose(outfile);
    free_list_elements(&list);
    return 0;
}

                
List create(char *line_of_ints, int len) {
    char *line = line_of_ints;

    char *tok = strtok(line, " ");
    int tok_read = 0;

    Node *_pre_head_sentinel = new_node(0);

    Node *cur = _pre_head_sentinel;

    while(tok != NULL) {
        tok_read ++;
        Node *next = new_node(atoi(tok));
        cur->next = next;

        cur = cur->next;
        tok = strtok(NULL, " ");
    }

    if (tok_read < len) {
        printf("Warning: Expected %d ints, found only %d.\n");
    }

    List list = {_pre_head_sentinel->next, cur};
    return list;
}

void print(FILE *fp, const List *list) {
    Node *cur = list->head;
    while(cur!=NULL) {
        fprintf(fp, "%d ", cur->data);
        cur = cur->next;
    }
    fprintf(fp, "\n");
}

int h_occur(const List *list) {
    int N = 101;
    Node *(hashtable[N]);
    for(int i = 0; i<N; i++) hashtable[i] = 0;

    Node *cur = list->head;
    while(cur != NULL) {
        int bucket = cur->data % N;
        if(hashtable[bucket] == NULL) {
            // printf("New: %d\n", cur->data);
            Node *node = new_node(cur->data);
            node->freq = 1;
            hashtable[bucket] = node;
        } else {
            Node *cur_bucket = hashtable[bucket];
            while(1) {
                if(cur_bucket->data == cur->data) {
                    cur_bucket->freq = cur_bucket->freq + 1;
                    // printf("Update: %d to %d\n", cur_bucket->data, cur_bucket->freq);
                    break;
                }

                if(cur_bucket->next == NULL) {
                    // printf("1New: %d\n", cur->data);
                    Node *node = new_node(cur->data);
                    node->freq = 1;
                    cur_bucket->next = node;
                    break;
                }

                cur_bucket = cur_bucket->next;
            }
        }
        cur = cur->next;
    }

    int max_freq = 0;
    int max_val = 0;
    for(int i = 0; i<N; i++) {
        Node *cur = hashtable[i];
        while(cur != NULL) {
            if(cur->freq>max_freq) {
                // printf("max_freq: %d; %d\n", cur->freq, cur->data);
                max_freq = cur->freq;
                max_val = cur->data;
            }
            cur = cur->next;
        }
    }
    return max_val;
}

    
void add_node(List *list, int data) {
    list->tail->next = new_node(data);
    list->tail = list->tail->next;
}


Node* new_node(int data) {
    Node *node = malloc(sizeof(Node));
    node->data = data;
    node->freq = INT_MIN;
    node->next = NULL;
    return node;
}

void free_list_elements(List *list) {
    Node *cur = list->head;
    while(cur != NULL) {
        Node *temp = cur;
        cur = cur->next;
        free(temp);
    }
}
