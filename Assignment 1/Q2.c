#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>

/* 02. Infix to postfix converter */

struct Stack {
    int *array;
    int size;
    int top;
};
typedef struct Stack Stack;

Stack * stack_create(int size);
void    stack_free(Stack *stack);
void    stack_push(Stack *stack, int val);
int     stack_pop(Stack *stack);
int     stack_peek(Stack *stack);
int     stack_is_empty(Stack *stack);


int priority(char op) {
    if(op=='+' || op == '-') return 0;
    if(op=='*' || op == '/') return 1;
    if(op=='^') return 2;
    return -1;
}

char* infix_to_postfix(char *exp) {
    static char outbuf[1000];
    int outbuf_len = 0;
    Stack *stack = stack_create(1000);
    for(int i=0, c=*exp; c!='\0'; i++, c=exp[i]) {
        if(isalnum(c)) {
            outbuf[outbuf_len] = c;
            outbuf_len++;
            continue;
        }

        if(c == '(') {
            // printf("push: %c\n", c);
            stack_push(stack, c);
            continue;
        }

        if(c==')') {
            char c = stack_pop(stack);
            while(c!='(') {
                // printf("append: %c\n", c);
                outbuf[outbuf_len] = c;
                outbuf_len++;
                c = stack_pop(stack);
            }
        }

        if(c == '+' || c=='-' || c=='*' || c=='/' || c=='^') {
            if(stack_is_empty(stack) || stack_peek(stack) == '(') {
                // printf("push: %c\n", c);
                stack_push(stack, c);
                continue;
            }

            while(1) {
                if(stack_is_empty(stack) || stack_peek(stack) == '(') {
                    stack_push(stack, c);
                    break;
                }
                char prev = stack_peek(stack);

                if(priority(c) < priority(prev)) {
                    // printf("push: %c\n", c);
                    stack_pop(stack);
                    outbuf[outbuf_len] = prev;
                    outbuf_len++;
                    continue;
                } else {
                    stack_push(stack, c);
                    break;
                }
            }
        
        }
    }

    while(!stack_is_empty(stack)) {
        outbuf[outbuf_len] = stack_pop(stack);
        outbuf_len++;
    }
    outbuf[outbuf_len] = '\0';
    stack_free(stack);
    return outbuf;
}



int main() {
    #define _buf_len 1024
    char buf[_buf_len];
    FILE *infile = fopen("input.txt", "r");
    FILE *outfile = fopen("output.txt", "w");

    int line_no = 0;
    while(fgets(buf, _buf_len, infile) != NULL) {
        line_no ++;
        // printf("%d: \n", line_no);
        char *out = infix_to_postfix(buf);
        // printf("%s\n", out);
        fprintf(outfile, "%s\n", out);
    }

    fclose(infile);
    fclose(outfile);
    return 0;
}


Stack *stack_create(int size) {
    Stack *stack = malloc(sizeof(Stack));
    stack->array = malloc(size *sizeof(int));
    stack->size = size;
    stack->top = -1;
    return stack;
}


void stack_free(Stack *stack) {
    free(stack->array);
    free(stack);
}


void    stack_push(Stack *stack, int val) {
    if(stack->top >= stack->size) {
        printf("W: Stackoverflow, can't push %d\n", val);
        return;
    }

    stack->top += 1;
    stack->array[stack->top] = val;
}


int     stack_pop(Stack *stack) {
    if(stack->top < 0) {
        printf("W: Stackunderflow\n");
        return 0;
    }
    int val = stack->array[stack->top];
    stack->top -= 1;
    return val;
}
int     stack_peek(Stack *stack) {
    if(stack->top < 0) {
        printf("W: Stackunderflow\n");
        return 0;
    }
    int val = stack->array[stack->top];
    return val;
}


int     stack_is_empty(Stack *stack) {
    return stack->top < 0;
}