#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/* Q6. Read BST from paranthesis notation, print height, diameter and width */

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
};
typedef struct Node Node;


Node * node_create();

Node * create_tree(char *s);
void free_tree(Node *root);

int    height(Node *root);
int    diameter(Node *root);
int    maxwidth(Node *root);

int main() {

    // FILE *infile = fopen("input.txt", "r");
    // FILE *outfile = fopen("output.txt", "w");
    FILE *infile = stdin;
    FILE *outfile = stdout;

    char BUF[1024];
    Node *root = NULL;
    while(fgets(BUF, 1024, infile) != NULL) {
        Node *root = create_tree(BUF);
        fprintf(outfile, "%d %d %d\n", height(root), diameter(root), maxwidth(root));
        free_tree(root);
    }

    fclose(infile);
    fclose(outfile);

    return 0;
}


Node * node_create() {
    Node *result = malloc(sizeof(Node));
    result->left = NULL;
    result->right = NULL;
    result->data = INT_MAX;
    return result;
}

Node * create_tree(char *str) {
    Node * pre_root = node_create(); // sentinel
    Node * cur = pre_root;
    Node * (parent_stack[256]);
    int parent_stack_top = -1;


    for(int i = 0; str[i]!='\0'; i++) {
        if((!isdigit(str[i])) && str[i] != '-' && str[i] != '(' && str[i] != ')') {
            continue;
        }

        if(isdigit(str[i])) {
            cur->data = atoi(str+i);
            // printf("data: %d\n", cur->data);
            while(isdigit(str[i])) i++;
            i--; continue;
        }

        if(str[i] == '(') {
            if(cur->left == NULL) {
                // printf("Left ");
                cur->left = node_create();
                parent_stack_top += 1;
                parent_stack[parent_stack_top] = cur;
                cur = cur->left;
            } else if (cur->right == NULL) {
                // printf("Right ");
                cur->right = node_create();
                parent_stack_top += 1;
                parent_stack[parent_stack_top] = cur;
                cur = cur->right;
            } else {
                printf("E: Unexpected child at %d-th char\n", i+1);
                return NULL;
            }
            continue;
        }

        if(str[i] == ')') {
            if(parent_stack_top < 0) {
                printf("E: Unmatched closing paranthesis at %d", i+1);
                return NULL;
            }
            // printf("Parent ");
            Node * parent = parent_stack[parent_stack_top];
            parent_stack_top --;

            cur = parent;
        }

    }

    return pre_root->left;
}

void free_tree(Node *root) {
    if(root==NULL) return;
    free_tree(root->left);
    free_tree(root->right);
    free(root);
}

int height(Node *root) {
    if(root == NULL||root->data==INT_MAX) {
        return -1;
    }
    int left_height = height(root->left);
    int right_height = height(root->right);
    if(left_height>right_height) {
        return 1+left_height;
    } else {
        return 1+right_height;
    }
}

int diameter(Node *root) {
    if(root==NULL||root->data==INT_MAX) {
        return 0;
    }
    
    int diameter_left_subtree = diameter(root->left);
    int diameter_right_subtree = diameter(root->right);
    int longest_path_throught_root = (height(root->left)+1) + (height(root->right)+1) + 1;

    int max_diameter_subtree;
    if(diameter_left_subtree>diameter_right_subtree) {
        max_diameter_subtree = diameter_left_subtree;
    } else {
        max_diameter_subtree = diameter_right_subtree;
    }

    if(max_diameter_subtree>longest_path_throught_root) {
        return max_diameter_subtree;
    } else {
        return longest_path_throught_root;
    }
}

int width(Node *root, int level) {
    if(root==NULL||root->data==INT_MAX) {
        return 0;
    }

    if(level == 0) {
        return 1;
    }

    return width(root->left, level-1) + width(root->right, level-1);
}

int maxwidth(Node *root) {
    int max_width = 0;
    for(int i = 0; i<=height(root); i++) {
        int cur_width = width(root, i);
        // printf("%d: %d\n",i,cur_width);
        if(cur_width>max_width) {
            max_width = cur_width;
        }
    }
    return max_width;
}

