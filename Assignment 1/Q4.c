#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/* Q6. Read BST from paranthesis notation, print height, diameter and width */

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
    struct Node *parent;
};
typedef struct Node Node;


Node * node_create();
void free_tree(Node *root);

void insert(Node **root, int elem);
void search(Node *root, int key);
void findMin(Node *root);
void findMax(Node *root);
void predecessor(Node *root, int elem);
void successor(Node *root, int elem);
void delete(Node **root, int data);
void inorder(Node *root);
void preorder(Node *root);
void postorder(Node *root);

FILE *INFILE;
FILE *OUTFILE;

int main() {

    // INFILE = fopen("input.txt", "r");
    // OUTFILE = fopen("output.txt", "w");
    INFILE = stdin;
    OUTFILE = stdout;

    char BUF[1024];
    Node *root = NULL;
    while(fgets(BUF, 1024, INFILE) != NULL) {
        char *op = strtok(BUF, " \n");
        printf("%s\n", op);
        if(strcmp(op, "stop") == 0) {
            free_tree(root);
            return 0;
        } else if(strcmp(op, "insr") == 0) {
            char *num_str = strtok(NULL, " ");
            if(num_str == NULL) {
                printf("E: Expected number after `insr`");
                return -1;
            }
            int num = atoi(num_str);
            insert(&root, num);
        } else if(strcmp(op, "srch") == 0) {
            char *num_str = strtok(NULL, " ");
            if(num_str == NULL) {
                printf("E: Expected number after `srch`");
                return -1;
            }
            int num = atoi(num_str);
            search(root, num);
        } else if(strcmp(op, "minm") == 0) {
            findMin(root);
        } else if(strcmp(op, "maxm") == 0) {
            findMax(root);
        } else if(strcmp(op, "pred") == 0) {
            char *num_str = strtok(NULL, " ");
            if(num_str == NULL) {
                printf("E: Expected number after `pred`");
                return -1;
            }
            int num = atoi(num_str);
            predecessor(root, num);
        } else if(strcmp(op, "succ") == 0) {
            char *num_str = strtok(NULL, " ");
            if(num_str == NULL) {
                printf("E: Expected number after `succ`");
                return -1;
            }
            int num = atoi(num_str);
            successor(root, num);
        } else if(strcmp(op, "delt") == 0) {
            char *num_str = strtok(NULL, " ");
            if(num_str == NULL) {
                printf("E: Expected number after `delt`\n");
                return -1;
            }
            int num = atoi(num_str);
            delete(&root, num);
        } else if(strcmp(op, "inor") == 0) {
            inorder(root);
        } else if(strcmp(op, "prer") == 0) {
            preorder(root);
        } else if(strcmp(op, "post") == 0) {
            postorder(root);
        } else {
            printf("E: Unknown command `%s`\n", op);
        }

    }

    fclose(INFILE);
    fclose(OUTFILE);

    return 0;
}


Node * node_create() {
    Node *result = malloc(sizeof(Node));
    result->left = NULL;
    result->right = NULL;
    result->parent = NULL;
    result->data = INT_MAX;
    return result;
}


void insert_rec(Node **node, Node *parent, int data) {
    if(*node==NULL) {
        *node = node_create();
        (*node)->data = data;
        (*node)->parent = parent;
        return;
    }
    Node *cur = *node;
    if(data < cur->data) {
        insert_rec(&cur->left, cur, data);
    } else {
        insert_rec(&cur->right, cur, data);
    }
}

void insert(Node **root, int elem) {
    insert_rec(root, NULL, elem);
}

void search(Node *root, int elem) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    Node *cur = root;
    while(cur!=NULL) {
        if(elem==cur->data) {
            fprintf(OUTFILE, "FOUND\n");
            return;
        }
        if(elem < cur->data) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    fprintf(OUTFILE, "NOT FOUND\n");
}

void findMin(Node *root) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    Node *cur = root;

    while(cur->left != NULL) {
        cur = cur->left;
    }

    fprintf(OUTFILE, "%d\n", cur->data);
}

void findMax(Node *root) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    Node *cur = root;

    while(cur->right != NULL) {
        cur = cur->right;
    }

    fprintf(OUTFILE, "%d\n", cur->data);
}

void predecessor(Node *root, int elem) {
    if(root == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return;
    }

    Node *cur = root;
    while(cur!=NULL) {
        if(elem==cur->data) {
            break;
        }
        if(elem < cur->data) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    if(cur == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return;
    }

    Node *left_child = cur->left;

    if(left_child == NULL) {
        fprintf(OUTFILE, "NIL");
        return;
    }

    Node *cur2 = left_child;
    while(cur2->right != NULL) {
        cur2 = cur2->right;
    }

    fprintf(OUTFILE, "%d\n", cur2->data);
}

void successor(Node *root, int elem) {
    if(root == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return;
    }

    Node *cur = root;
    while(cur!=NULL) {
        if(elem==cur->data) {
            break;
        }
        if(elem < cur->data) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    if(cur == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return;
    }

    Node *right_child = cur->right;

    if(right_child == NULL) {
        fprintf(OUTFILE, "NIL");
        return;
    }

    Node *cur2 = right_child;
    while(cur2->left != NULL) {
        cur2 = cur2->left;
    }

    fprintf(OUTFILE, "%d\n", cur2->data);
}

void inorder(Node *root) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    if(root->left != NULL) {
        inorder(root->left);
    }
    fprintf(OUTFILE, "%d ", root->data);
    if(root->right != NULL) {
        inorder(root->right);
    }
}

void preorder(Node *root) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    fprintf(OUTFILE, "%d ", root->data);
    if(root->left != NULL) {
        preorder(root->left);
    }
    if(root->right != NULL) {
        preorder(root->right);
    }
}

void postorder(Node *root) {
    if(root == NULL) {
        fprintf(OUTFILE, "NIL\n");
        return;
    }

    if(root->left != NULL) {
        postorder(root->left);
    }
    if(root->right != NULL) {
        postorder(root->right);
    }
    fprintf(OUTFILE, "%d ", root->data);
}

void transplant(Node **root, Node *u, Node *v) {
    if (u->parent == NULL) {
        *root = v;
    } else if (u == u->parent->left) {
        u->parent->left = v;
    } else {
        u->parent->right = v;
    }
    if(v!=NULL) {
        v->parent = u->parent;
    }
}

Node *tree_min(Node *root) {
    Node *cur = root;
    while(cur->left != NULL) cur = cur->left;
    return cur;
}

void delete(Node **root, int data) {

    Node *cur = *root;
    while(cur!=NULL) {
        if(data==cur->data) {
            break;
        }
        if(data < cur->data) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    if(cur == NULL) {
        fprintf(OUTFILE, "NOT FOUND\n");
        return;
    }

    if(cur->left == NULL) {
        transplant(root, cur, cur->right);
    } else if(cur->right == NULL) {
        transplant(root, cur, cur->left);
    } else {
        Node *min = tree_min(cur->right);
        if(min->parent != cur) {
            transplant(root, min, min->right);
            min->right = cur->right;
            min->right->parent = min;
        }
        transplant(root, cur, min);
        min->left = cur->left;
        min->left->parent = min;
    }
}

void free_tree(Node *root) {
    if(root == NULL) return;
    free_tree(root->left);
    free_tree(root->right);
    free(root);
}