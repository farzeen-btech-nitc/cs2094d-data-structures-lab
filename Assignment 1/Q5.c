#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/* Q5. Read preorder listing of a BST and print it back in paranthesis notation */

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
};
typedef struct Node Node;


Node * node_create();

Node * create_tree(int *A, int N);
void   print_tree(FILE *fp, const Node * root);

int main() {

    // FILE *infile = fopen("input.txt", "r");
    // FILE *outfile = fopen("output.txt", "w");
    FILE *infile = stdin;
    FILE *outfile = stdout;

    char BUF[1024];

    fgets(BUF, 1024, infile);
    int N = atoi(BUF);
    
    fgets(BUF, 1024, infile);

    int nums[N];
    int i = 0;
    char *tok = strtok(BUF, " ");
    while(tok != NULL) {
        nums[i] = atoi(tok);
        tok = strtok(NULL, " ");
        i++;
    }

    if(i!=N) {
        printf("E: Expected %d items, got only %d\n", N, i);
        return -1;
    }

    Node* root = create_tree(nums, N);
    print_tree(outfile, root);

    return 0;
}


Node * node_create() {
    Node *result = malloc(sizeof(Node));
    result->left = NULL;
    result->right = NULL;
    result->data = INT_MAX;
    return result;
}


Node * create_tree(int *A, int N) {
    if(N==0) return NULL;

    Node *root = node_create();
    root->data = A[0];

    if(N==1) return root;

    int i = 1;
    while(A[i]<A[0] && i<N) i++;

    /**
     * Now `i` contains the index of first element larger than A[0]
     * or `i` is equal to `N`
     **/

    int len_left_subtree = i-1;
    int len_right_subtree =  N-len_left_subtree-1;

    int *left_subtree = A+1;
    int *right_subtree = A+i;

    root->left = create_tree(left_subtree, len_left_subtree);
    root->right = create_tree(right_subtree, len_right_subtree);

    return root;
}

void print_tree(FILE *fp, const Node* root) {
    fprintf(fp, "(");
    if(root!=NULL && root->data != INT_MAX) {
        fprintf(fp, "%d", root->data);
        print_tree(fp, root->left);
        print_tree(fp, root->right);
    }
    fprintf(fp, ")");
}