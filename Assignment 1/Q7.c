#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/* Q7. Read BST from paranthesis notation, mirror and print */

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
};
typedef struct Node Node;


Node * node_create();

Node * create_tree(char *s);
void   free_tree(Node *root);

void   print_tree(FILE *fp, const Node * root);
void   mirror_tree(Node * root);

int main() {

    // FILE *infile = fopen("input.txt", "r");
    // FILE *outfile = fopen("output.txt", "w");
    FILE *infile = stdin;
    FILE *outfile = stdout;

    char BUF[1024];
    Node *root = NULL;
    while(fgets(BUF, 1024, infile) != NULL) {
        switch(BUF[0]) {
            case 'c':
                root = create_tree(BUF+1);
                if(root==NULL) return -1;
                break;
            case 'p':
                print_tree(outfile, root);
                fprintf(outfile, "\n");
                break;
            case 'm':
                mirror_tree(root);
                break;
            case 's':
                free_tree(root);
                fclose(infile);
                fclose(outfile);
                return 0;
        }

    }
    // printf("END");

    free_tree(root);
    fclose(infile);
    fclose(outfile);
    return 0;
}


Node * node_create() {
    Node *result = malloc(sizeof(Node));
    result->left = NULL;
    result->right = NULL;
    result->data = INT_MAX;
    return result;
}

Node * create_tree(char *str) {
    Node * pre_root = node_create(); // sentinel
    Node * cur = pre_root;
    Node * (parent_stack[256]);
    int parent_stack_top = -1;


    for(int i = 0; str[i]!='\0'; i++) {
        if((!isdigit(str[i])) && str[i] != '-' && str[i] != '(' && str[i] != ')') {
            continue;
        }

        if(isdigit(str[i])) {
            cur->data = atoi(str+i);
            // printf("data: %d\n", cur->data);
            while(isdigit(str[i])) i++;
            i--; continue;
        }

        if(str[i] == '(') {
            if(cur->left == NULL) {
                // printf("Left ");
                cur->left = node_create();
                parent_stack_top += 1;
                parent_stack[parent_stack_top] = cur;
                cur = cur->left;
            } else if (cur->right == NULL) {
                // printf("Right ");
                cur->right = node_create();
                parent_stack_top += 1;
                parent_stack[parent_stack_top] = cur;
                cur = cur->right;
            } else {
                printf("E: Unexpected child at %d-th char\n", i+1);
                return NULL;
            }
            continue;
        }

        if(str[i] == ')') {
            if(parent_stack_top < 0) {
                printf("E: Unmatched closing paranthesis at %d", i+1);
                return NULL;
            }
            // printf("Parent ");
            Node * parent = parent_stack[parent_stack_top];
            parent_stack_top --;

            cur = parent;
        }

    }

    return pre_root->left;
}

void free_tree(Node *root) {
    if(root==NULL) return;
    free_tree(root->left);
    free_tree(root->right);
    free(root);
}

void mirror_tree(Node *root) {
    Node *temp = root->left;
    root->left = root->right;
    root->right = temp;

    if(root->left != NULL) mirror_tree(root->left);
    if(root->right != NULL) mirror_tree(root->right);
}

void print_tree(FILE *fp, const Node* root) {
    fprintf(fp, "(");
    if(root!=NULL && root->data != INT_MAX) {
        fprintf(fp, "%d", root->data);
        if(root->left !=NULL||root->right!=NULL) {
            print_tree(fp, root->left);
            print_tree(fp, root->right);
        }
    }
    fprintf(fp, ")");
}