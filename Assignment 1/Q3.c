#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>

/* 03. Postfix evaluator */

struct Stack {
    int *array;
    int size;
    int top;
};
typedef struct Stack Stack;

Stack * stack_create(int size);
void    stack_free(Stack *stack);
void    stack_push(Stack *stack, int val);
int     stack_pop(Stack *stack);
int     stack_is_empty(Stack *stack);


int evaluatePostfix(char *exp) {
    Stack *stack = stack_create(1000);

    int number = -1;
    char c = *exp;
    for(int i=0, c=*exp; c!='\0'; i++, c=exp[i]) {
        if(isdigit(c)) {
            if(number == -1) {
                number = 0;
            }
            number *= 10;
            number += c - '0';
            continue;
        }
        
        if(number != -1) {
            stack_push(stack, number);
            number = -1;
        }

        if(c == '+' || c=='-' || c=='*' || c=='/' || c=='^') {
            int op2 = stack_pop(stack);
            int op1 = stack_pop(stack);
            int res = 0;
            switch(c) {
                case '+':
                    res = op1 + op2;
                    break;
                case '-':
                    res = op1 - op2;
                    break;
                case '*':
                    res = op1 * op2;
                    break;
                case '/':
                    res = op1 / op2;
                    break;
                case '^':
                    res = pow(op1, op2);
                    break;
            }
            printf("%d %c %d: %d\n", op1, c, op2, res);
            stack_push(stack, res);
        }
    }

    int result = stack_pop(stack);
    if(!stack_is_empty(stack)) {
        printf("W: Stack not empty.\n");
        stack_free(stack);
        return INT_MAX;
    }

    stack_free(stack);
    return result;
}



int main() {
    #define _buf_len 1024
    char buf[_buf_len];
    FILE *infile = fopen("input.txt", "r");
    FILE *outfile = fopen("output.txt", "w");

    int line_no = 0;
    while(fgets(buf, _buf_len, infile) != NULL) {
        line_no ++;
        fprintf(outfile, "%d\n",evaluatePostfix(buf));
    }

    fclose(infile);
    fclose(outfile);
    return 0;
}


Stack *stack_create(int size) {
    Stack *stack = malloc(sizeof(Stack));
    stack->array = malloc(size *sizeof(int));
    stack->size = size;
    stack->top = -1;
    return stack;
}


void stack_free(Stack *stack) {
    free(stack->array);
    free(stack);
}


void    stack_push(Stack *stack, int val) {
    if(stack->top >= stack->size) {
        printf("W: Stackoverflow, can't push %d\n", val);
        return;
    }

    stack->top += 1;
    stack->array[stack->top] = val;
}


int     stack_pop(Stack *stack) {
    if(stack->top < 0) {
        printf("W: Stackunderflow\n");
        return 0;
    }
    int val = stack->array[stack->top];
    stack->top -= 1;
    return val;
}


int     stack_is_empty(Stack *stack) {
    return stack->top < 0;
}