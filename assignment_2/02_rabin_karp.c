#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE* infile;
FILE* outfile;

struct State {
    char* text;
    char* pat;
};
typedef struct State State;

void text(State* state, char* buf);
void pattern(State* state, char* buf);
void print(State* state, FILE* outfile);

int main() {
    #define DEBUG
    #ifdef DEBUG
    infile = stdin;
    outfile = stdout;
    #else
    infile = fopen("input.txt", "r");
    outfile = fopen("output.txt", "w");
    #endif

    State state;
    state.text = NULL;
    state.pat = NULL;

    char buf[1024];
    while(fgets(buf, 1024, infile) != NULL) {
        if(buf[0] == 't') {
            text(&state, buf);
        } else if(buf[0] == 'p' && buf[1] == 'r') {
            print(&state, outfile);
        } else if(buf[0] == 'p') {
            pattern(&state, buf);
        } else {
            break;
        }
    }

    if(state.text != NULL) free(state.text);
    if(state.pat != NULL) free(state.pat);

    #ifndef DEBUG
    fclose(infile);
    fclose(outfile);
    #endif

    return 0;
}

char* get_quote_content(char* s) {
    char* result = malloc(sizeof(char)*strlen(s));
    while(*s != '"') s++;
    s++;
    int i = 0;
    while(s[i] != '"') {
        result[i] = s[i];
        i++;
    }
    result[i] = '\0';
    return result;
}

void text(State *state, char* buf) {
    if(state->text != NULL) free(state->text);
    char* s = get_quote_content(buf);
    state->text = s;
}

void pattern(State *state, char* buf) {
    if(state->pat != NULL) free(state->pat);
    char* s = get_quote_content(buf);
    state->pat = s;
}

void print(State *state, FILE* outfile) {
    assert(state->text != NULL);
    assert(state->pat != NULL);

    int b = 256;
    int m = 101;
    char* pat = state->pat;
    char* text= state->text;
    int len_pat = strlen(pat);
    int len_text = strlen(text);

    // leading digit multiplier: 
    int msd_mul = 1;
    for(int i = 0; i<len_pat-1; i++) {
        msd_mul = msd_mul*b % m;
    }

    // Hash pattern and first slice
    int h_pat = 0;
    int h_text = 0;
    for(int i = 0; i<len_pat; i++) {
        h_pat = (h_pat*b + pat[i]) % m;
        h_text = (h_text*b + text[i]) % m;
    }

    int found = 0;

    for(int index = 0; index <= len_text-len_pat; index++) {
        if(h_pat == h_text) {
            int is_same = 1;
            for(int i = 0; i< len_pat; i++) {
                if(pat[i] != text[index+i]) {
                    is_same = 0;
                    break;
                }
            }
            if(is_same) {
                found = 1;
                fprintf(outfile, "%d ", index);
            }
        }
        h_text = ((h_text - msd_mul*text[index])*b + text[index+len_pat]) % m;
        if(h_text<0) h_text += m;
    }
    if(!found) fprintf(outfile, "-1");
    fprintf(outfile, "\n");
}
