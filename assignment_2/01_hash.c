#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct HTNode {
    int v;
    struct HTNode* next;
};
typedef struct HTNode HTNode;

struct HTChain {
    HTNode* array[10240];
    int size;
};
typedef struct HTChain HTChain;

typedef int fn_hash(int k, int i, void *data);

struct HTOpenAddr {
    int array[10240];
    int size;

    // Polymorphic
    fn_hash* hash;
    void*    hash_data;
};
typedef struct HTOpenAddr HTOpenAddr;


/* Hash functions */

struct HashLinear {
    int m;
};
typedef struct HashLinear HashLinear;

struct HashQuad {
    int c1;
    int c2;
    int m;
};
typedef struct HashQuad HashQuad;

struct HashDouble {
    int R;
    int m;
};
typedef struct HashDouble HashDouble;

int hash_linear(int k, int i, void* data);
int hash_quad(int k, int i, void *data);
int hash_double(int k, int i, void *data);


HTChain* ht_chain_create(int size);
void ht_chain_insert(HTChain* ht, int e);
int  ht_chain_search(HTChain* ht, int e);
void ht_chain_delete(HTChain* ht, int e);
void ht_chain_print(HTChain *ht);

HTOpenAddr* ht_linear_create(int size);
HTOpenAddr* ht_quad_create(int size, int c1, int c2);
HTOpenAddr* ht_dh_create(int size);

void ht_open_addr_insert(HTOpenAddr* ht, int e);
int  ht_open_addr_search(HTOpenAddr* ht, int e);
void ht_open_addr_delete(HTOpenAddr* ht, int e);
void ht_open_addr_print(HTOpenAddr *ht);

/* Polymorphism */
typedef void fn_ht_insert(void* HT, int e);
typedef int  fn_ht_search(void* HT, int e);
typedef void fn_ht_delete(void* HT, int e);
typedef void fn_ht_print(void* HT);


FILE* infile;
FILE* outfile;

int main() {
    #define DEBUG
    #ifdef DEBUG
    infile = stdin;
    outfile = stdout;
    #else
    infile = fopen("input.txt", "r");
    outfile = fopen("output.txt", "w");
    #endif

    void* ht = NULL;
    fn_ht_insert* ht_insert = NULL;
    fn_ht_search* ht_search = NULL;
    fn_ht_delete* ht_delete = NULL;
    fn_ht_print*  ht_print  = NULL;

    char buf[1024];
    // First line
    fgets(buf, 1024, infile);
    char c = buf[0];

    fgets(buf, 1024, infile);
    int size = atoi(buf);

    if(c == 'd') {
        // Chaining
        ht = ht_chain_create(size);
        ht_insert = (fn_ht_insert*) ht_chain_insert;
        ht_search = (fn_ht_search*) ht_chain_search;
        ht_delete = (fn_ht_delete*) ht_chain_delete;
        ht_print  = (fn_ht_print *) ht_chain_print;
    } else {
        ht_insert = (fn_ht_insert*) ht_open_addr_insert;
        ht_search = (fn_ht_search*) ht_open_addr_search;
        ht_delete = (fn_ht_delete*) ht_open_addr_delete;
        ht_print  = (fn_ht_print*) ht_open_addr_print;
        switch(c) {
            case 'a':
                // Linear probing: h2(k, i) = (h1(k) + i) % size;
                ht = ht_linear_create(size);
                break;

            case 'b':
                // Quadratic probing: h2(k, i) = (h1(k) + c1*i + c2*i^2) % size;
                fgets(buf, 1024, infile);
                char *tok = strtok(buf, " \n");
                int c1 = atoi(tok);
                tok = strtok(NULL, " \n");
                int c2 = atoi(tok);

                ht = ht_quad_create(size, c1, c2);
                break;

            case 'c':
                // Double hashing: h(k, i) = (h1(k) + i*h2(k)) % size
                //                 h2(k) = R - (k % R); R is the largest prime less than size
                ht = ht_dh_create(size);
                break;

            case 'd':
                // Chaining :)
                break;
        }
    }

    while(fgets(buf, 1024, infile) != NULL) {
        int x;
        switch(buf[0]) {
            case 'i':
                x = atoi(buf+1);
                ht_insert(ht, x);
                break;

            case 's':
                x = atoi(buf+1);
                if(ht_search(ht, x)) {
                    printf("1\n");
                } else {
                    printf("-1\n");
                }
                break;
            
            case 'd':
                x = atoi(buf+1);
                ht_delete(ht, x);
                break;

            case 'p':
                ht_print(ht);
                break;
            
            default:
                goto cleanup;
        }
    }

    cleanup:

    #ifndef DEBUG
    fclose(infile);
    fclose(outfile);
    #endif

    return 0;
}


HTChain* ht_chain_create(int size) {
    HTChain* result = malloc(sizeof(HTChain));
    result->size = size;
    for(int i = 0; i<result->size; i++) {
        result->array[i] = NULL;
    }
    return result;
}


void ht_chain_insert(HTChain* ht, int e) {
    int h = e % ht->size;
    HTNode* top = ht->array[h];
    ht->array[h] = malloc(sizeof(HTNode));
    ht->array[h]->next = top;
    ht->array[h]->v = e;
}

int ht_chain_search(HTChain* ht, int e) {
    int h = e % ht->size;
    HTNode* top = ht->array[h];
    while(top != NULL && top->v != e) top = top->next;
    return top != NULL;
}


void ht_chain_delete(HTChain* ht, int e) {
    int h = e % ht->size;
    HTNode* top = ht->array[h];
    HTNode* prev = NULL;

    while(top != NULL && top->v != e) {
        prev = top;
        top = top->next;
    }

    assert(top != NULL);
    prev->next = top->next;
    free(top);
}


void ht_chain_print(HTChain* ht) {
    for(int i = 0; i<ht->size; i++) {
        fprintf(outfile, "%d (", i);
        HTNode* top = ht->array[i];
        while(top!=NULL) {
            fprintf(outfile, "%d ", top->v);
            top = top->next;
        }
        fprintf(outfile, ")\n");
    }
}


HTOpenAddr* ht_linear_create(int size) {
    HTOpenAddr* result = malloc(sizeof(HTOpenAddr));
    result->size = size;
    for(int i = 0; i<size; i++) {
        // sentinel for empty slot
        result->array[i] = INT_MAX;
    }
    result->hash = hash_linear;
    HashLinear* data = malloc(sizeof(HashLinear));
    data->m = size;
    result->hash_data = data;
    return result;
}

HTOpenAddr* ht_quad_create(int size, int c1, int c2) {
    HTOpenAddr* result = malloc(sizeof(HTOpenAddr));
    result->size = size;
    for(int i = 0; i<size; i++) {
        // sentinel for empty slot
        result->array[i] = INT_MAX;
    }
    result->hash = hash_quad;
    HashQuad* quad_data = malloc(sizeof(HashQuad));
    quad_data->c1 = c1;
    quad_data->c2 = c2;
    quad_data->m = size;
    result->hash_data = quad_data;
    return result;
}

HTOpenAddr* ht_dh_create(int size) {
    HTOpenAddr* result = ht_linear_create(size);
    result->hash = hash_double;
    HashDouble* hash_data = malloc(sizeof(HashDouble));
    for(int i = size; i>=2; i++) {
        int is_prime = 1;
        for(int j = 2; j<=i/2; j++) {
            if(i % j == 0) {
                is_prime = 0;
                break;
            }
        }
        if(is_prime) {
            hash_data->R = i;
            break;
        }
    }
    hash_data->m = size;
    result->hash_data = hash_data;
    return result;
}


void ht_open_addr_insert(HTOpenAddr* ht, int e) {
    int i = 0;
    for(; i<ht->size; i++) {
        int h = ht->hash(e, i, ht->hash_data);
        if(ht->array[h] == INT_MAX) {
            ht->array[h] = e;
            break;
        }
    }
    if(i == ht->size) {
        fprintf(outfile, "Error: Hashtable is full\n");
    }
}


int ht_open_addr_search(HTOpenAddr* ht, int e) {
    int i = 0;
    for(; i<ht->size; i++) {
        int h = ht->hash(e, i, ht->hash_data);
        if(ht->array[h] == e) return 1;
        if(ht->array[h] == INT_MAX) break;
    }
    return 0;
}


void ht_open_addr_delete(HTOpenAddr* ht, int e) {
    int i = 0;
    int found_index = -1;
    for(; i<ht->size; i++) {
        int h = ht->hash(e, i, ht->hash_data);
        if(ht->array[h] == e) {
            found_index = i;
            break;
        }
        if(ht->array[h] == INT_MAX) break;
    }
    if(found_index != -1) {
        for(int i = found_index; i<ht->size; i++) { 
            int h = ht->hash(e, i, ht->hash_data);
            int h_ = ht->hash(e, i+1, ht->hash_data);
            ht->array[h] = ht->array[h_];
            if(ht->array[h] == INT_MAX) break;
        }
    }
}


void ht_open_addr_print(HTOpenAddr* ht) {
    for(int i = 0; i<ht->size; i++) {
        fprintf(outfile, "%d (", i);
        if(ht->array[i] != INT_MAX) {
            fprintf(outfile, "%d", ht->array[i]);
        }
        fprintf(outfile, ")\n");
    }
}

int hash_linear(int k, int i, void* data) {
    HashLinear* hash_data = data;
    return ( (k % hash_data->m) + i ) % hash_data->m;
}

int hash_quad(int k, int i, void* data) {
    HashQuad* hash_data = data;
    return ( (k % hash_data->m) + hash_data->c1*i + hash_data->c2*i*i ) % hash_data->m;
}

int hash_double(int k, int i, void* data) {
    HashDouble* hash_data = data;
    return ( (k % hash_data->m) + i*(hash_data->R - (k%hash_data->R))) % hash_data->m;
}