#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE* infile;
FILE* outfile;

struct Node {
    int a;
    int b;
    struct Node* next;
    int has_symmetric_pair;
};
typedef struct Node Node;

struct HashTable {
    Node** ptr_array;
    int size;
};
typedef struct HashTable HashTable;

void ht_initialize(HashTable* ht, int size);
void ht_insert(HashTable* ht, int a, int b);
void ht_free(HashTable *ht);

void print_symmetric(HashTable *ht);

int main() {
    #define DEBUG
    #ifdef DEBUG
    infile = stdin;
    outfile = stdout;
    #else
    infile = fopen("input.txt", "r");
    outfile = fopen("output.txt", "w");
    #endif

    HashTable ht;
    ht_initialize(&ht, 101);

    char buf[1024];
    while(fgets(buf, 1024, infile) != NULL) {
        if(buf[0] == 's') {
            char* tok = strtok(buf, "() ,");
            tok = strtok(NULL, "() ,");
            int a = atoi(tok);
            tok = strtok(NULL, "() ,");
            int b = atoi(tok);
            ht_insert(&ht, a, b);
        } else if(buf[0] == 'p') {
            print_symmetric(&ht);
        } else {
            break;
        }
    }


    ht_free(&ht);
    #ifndef DEBUG
    fclose(infile);
    fclose(outfile);
    #endif

    return 0;
}

void ht_initialize(HashTable* ht, int size) {
    ht->ptr_array = calloc(size, sizeof(Node*));
    ht->size = size;
}

void ht_insert(HashTable *ht, int a, int b) {
    int h = (a*b) % ht->size;
    Node* node = malloc(sizeof(Node));
    node->a = a;
    node->b = b;
    node->has_symmetric_pair = 0;
    node->next = ht->ptr_array[h];
    ht->ptr_array[h] = node;

    Node* cur = node->next;
    while(cur!=NULL) {
        if(node->a == cur->b && node->b == cur->a) {
            node->has_symmetric_pair = 1;
            cur->has_symmetric_pair = 1;
        }
        cur = cur->next;
    }
}

void print_symmetric(HashTable *ht) {
    for(int i = 0; i<ht->size; i++) {
        Node* cur = ht->ptr_array[i];
        while(cur != NULL) {
            if(cur->has_symmetric_pair) {
                fprintf(outfile, "(%d, %d)\n", cur->a, cur->b);
            }
            cur = cur->next;
        }
    }
}

void __ht_free_aux(Node* n) {
    if(n==NULL) return;
    __ht_free_aux(n->next);
    free(n);
}

void ht_free(HashTable *ht) {
    for(int i = 0; i<ht->size; i++) {
        __ht_free_aux(ht->ptr_array[i]);
    }
    free(ht->ptr_array);
}
