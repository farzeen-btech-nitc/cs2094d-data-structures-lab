#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *INFILE;
FILE *OUTFILE;

#define COLOR_WHITE 'W'
#define COLOR_GRAY 'G'
#define COLOR_BLACK 'B'

struct Vertex;

struct Node {
    struct Vertex *v;
    struct Node *next;
};
typedef struct Node Node;

struct Vertex {
    int x;
    char color;
    struct Node *adj_list;
};
typedef struct Vertex Vertex;


void bfs(Vertex *start, int end);
int dfs(Vertex *start, int end);

void color_white(Vertex *vertices, int n) {
    for (int i = 0; i < n; i++) {
        vertices[i].color = COLOR_WHITE;
    }
}

int main() {

    // INFILE = fopen("input.txt", "r");
    // OUTFILE = fopen("output.txt", "w");
    INFILE = stdin;
    OUTFILE = stdout;

    char BUF[1024];
    fgets(BUF, 1024, INFILE);
    int n = atoi(BUF);
    Vertex vertices[n];
    for (int i = 0; i < n; i++) {
        fgets(BUF, 1024, INFILE);
        Vertex *v = &vertices[i];
        v->x = i;
        // Head sentinel
        v->adj_list = malloc(sizeof(Node));
        v->adj_list->next = NULL;
        v->adj_list->v = v;

        Node *cur = v->adj_list;

        for (char *tok = strtok(BUF, " "); tok != NULL;
             tok = strtok(NULL, " ")) {
            cur->next = malloc(sizeof(Node));
            cur->next->v = &vertices[atoi(tok)];
            cur->next->next = NULL;
            cur = cur->next;
        }
    }

    while (fgets(BUF, 1024, INFILE) != NULL) {
        char *action = strtok(BUF, " ");
        if (strcmp(action, "bfs") == 0) {
            int x = atoi(strtok(NULL, " "));
            int y = atoi(strtok(NULL, " "));
            color_white(vertices, n);
            bfs(&vertices[x], y);
            fprintf(OUTFILE, "\n");
        } else if (strcmp(action, "dfs") == 0) {
            int x = atoi(strtok(NULL, " "));
            int y = atoi(strtok(NULL, " "));
            color_white(vertices, n);
            dfs(&vertices[x], y);
            fprintf(OUTFILE, "\n");
        } else if (strcmp(action, "stp") == 0) {
            return 0;
        } else {
            continue;
        }
    }

    fclose(INFILE);
    fclose(OUTFILE);

    return 0;
}


void bfs(Vertex *v, int end) {

    Vertex *(q[1000]);
    int q_top = -1;

    q_top += 1;
    q[q_top] = v;

    fprintf(OUTFILE, "%d ", v->x);
    while (q_top > -1) {
        Vertex *u = q[q_top];
        q_top--;

        for (Node *i = u->adj_list->next; i != NULL; i = i->next) {
            Vertex *w = i->v;
            if (w->color == COLOR_WHITE) {
                fprintf(OUTFILE, "%d ", w->x);
                if (w->x == end) {
                    return;
                }
                w->color = COLOR_GRAY;
                q_top += 1;
                q[q_top] = w;
            }
        }
        u->color = COLOR_BLACK;
    }
}

int dfs(Vertex *v, int end) {
    if(v->color != COLOR_WHITE)
        return 0;

    v->color = COLOR_GRAY;
    fprintf(OUTFILE, "%d ", v->x);
    if (v->x == end) {
        return 1;
    }

    for (Node *cur = v->adj_list->next; cur != NULL; cur = cur->next) {
        if (dfs(cur->v, end))
            return 1;
    }
    v->color = COLOR_BLACK;
    return 0;
}