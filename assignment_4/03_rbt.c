#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Red Black Tree */

#define COLOR_RED 0
#define COLOR_BLACK 1
struct Node {
    int data;
    int color;
    struct Node *left;
    struct Node *right;
    struct Node *parent;
};
typedef struct Node Node;

struct RBTree {
    struct Node *root;
    struct Node *NIL;
};
typedef struct RBTree RBTree;

RBTree *rbt_new();
void rbt_insert(RBTree *tree, int data);
void _rotate_left(RBTree *tree, Node *x);
void _rotate_right(RBTree *tree, Node *x);
void rbt_print(const RBTree *tree, const Node *root);

FILE *INFILE;
FILE *OUTFILE;

int main() {

    // INFILE = fopen("input.txt", "r");
    // OUTFILE = fopen("output.txt", "w");
    INFILE = stdin;
    OUTFILE = stdout;

    char BUF[1024];
    RBTree *tree = rbt_new();
    while (fgets(BUF, 1024, INFILE) != NULL) {
        int n = atoi(BUF);
        rbt_insert(tree, n);
    }

    fclose(INFILE);
    fclose(OUTFILE);

    return 0;
}

RBTree *rbt_new() {
    RBTree *ret = malloc(sizeof(RBTree));
    ret->NIL = malloc(sizeof(Node));
    ret->NIL->color = COLOR_BLACK;
    ret->NIL->data = 0;
    ret->NIL->left = NULL;
    ret->NIL->right = NULL;
    ret->NIL->parent = NULL;
    ret->root = ret->NIL;

    return ret;
}

void _rotate_left(RBTree *tree, Node *x) {
    /*
         x                  y
        / \                / \
      .a   y     ===>     x  .z
          / \            / \
         b  .z         .a   b
    */

    Node *y = x->right;
    Node *b = y->left;

    if (x == tree->root) {
        tree->root = y;
        y->parent = tree->NIL;
    } else {
        if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }
        y->parent = x->parent;
    }

    x->right = b;
    if (b)
        b->parent = x;

    y->left = x;
    x->parent = y;
}

void _rotate_right(RBTree *tree, Node *x) {
    /*
           x              y
          / \            / \
         y  .a  ===>   .z   x
        / \                / \
      .z   b              b  .a
    */
    Node *y = x->left;
    Node *b = y->right;

    if (x == tree->root) {
        tree->root = y;
        y->parent = tree->NIL;
    } else {
        if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }
        y->parent = x->parent;
    }

    x->left = b;
    if (b)
        b->parent = x;

    y->right = x;
    x->parent = y;
}


Node *_new_node(const RBTree *tree, int data) {
    Node *node = malloc(sizeof(Node));
    node->data = data;
    node->color = COLOR_RED;
    node->parent = tree->NIL;
    node->left = tree->NIL;
    node->right = tree->NIL;
    return node;
}

int _rbt_fixup(RBTree *tree, Node *z) {
    int ret = 0;
    while (z->parent->color == COLOR_RED) {
        ret = 1;
        Node *p = z->parent;
        Node *g = z->parent->parent;

        if(g->left == p) {
            if(g->right->color == COLOR_RED) {
                g->right->color = COLOR_BLACK;
                g->left->color = COLOR_BLACK;
                g->color = COLOR_RED;
                z = g;
            } else {
                if(p->right == z) {
                    _rotate_left(tree, p);
                    z = p;
                    p = z->parent;
                }
                _rotate_right(tree, g);
                g->color = COLOR_RED;
                p->color = COLOR_BLACK;
            }
        } else {
            if(g->left->color == COLOR_RED) {
                g->left->color = COLOR_BLACK;
                g->right->color = COLOR_BLACK;
                g->color = COLOR_RED;
                z = g;
            } else {
                if(p->left == z) {
                    _rotate_right(tree, p);
                    z = p;
                    p = z->parent;
                }
                _rotate_left(tree, g);
                g->color = COLOR_RED;
                p->color = COLOR_BLACK;
            }
        }

    }
    if (tree->root->color == COLOR_RED) {
        tree->root->color = COLOR_BLACK;
    }
    return ret;
}

void rbt_insert(RBTree *tree, int data) {
    Node *node = _new_node(tree, data);
    if (tree->root == tree->NIL) {
        tree->root = node;
        node->color = COLOR_BLACK;
    } else {
        Node *cur = tree->root;
        while (1) {
            if (data < cur->data) {
                if (cur->left == tree->NIL) {
                    node->parent = cur;
                    cur->left = node;
                    break;
                } else {
                    cur = cur->left;
                }
            } else {
                if (cur->right == tree->NIL) {
                    node->parent = cur;
                    cur->right = node;
                    break;
                } else {
                    cur = cur->right;
                }
            }
        }
    }

    rbt_print(tree, tree->root);
    fprintf(OUTFILE, "\n");
    if (_rbt_fixup(tree, node)) {
        rbt_print(tree, tree->root);
        fprintf(OUTFILE, "\n");
    }
}

void rbt_print(const RBTree *tree, const Node *root) {
    if (root == tree->NIL)
        return;
    fprintf(OUTFILE, "(");
    rbt_print(tree, root->left);
    fprintf(OUTFILE, "%d%c", root->data,
            root->color == COLOR_BLACK ? 'B' : 'R');
    rbt_print(tree, root->right);
    fprintf(OUTFILE, ")");
}
