#!/usr/bin/env python3
import os
import sys
import subprocess as sp

# TO BE SET BY USER
roll_no = "B170815CS"
first_name = "MUHAMMED"

cur_dir_name: str = os.getcwd().split('/')[-1]
print("Current folder: {}".format(cur_dir_name))
if not cur_dir_name.startswith("assignment_"):
    print("Not in assignment_* directory. Exiting.")
    sys.exit(0)


assignment_num = cur_dir_name.split('_')[1]
zip_folder_name = "ASSG{assignment_num}_{roll_no}_{first_name}.zip".format(
                                                assignment_num=assignment_num,
                                                roll_no=roll_no,
                                                first_name=first_name)
print("Zip name: {}".format(zip_folder_name))

orig_file_names = os.listdir()
files_to_zip = []  # list of tuples (origname, newname)
for orig_name in orig_file_names:
    if not orig_name.endswith('.c'):
        continue
    qn_number = orig_name.split('_')[0].upper()
    new_name = "ASSG{assignment_num}_{roll_no}_{first_name}_{qn_number}.c" \
        .format(assignment_num=assignment_num, roll_no=roll_no,
                first_name=first_name, qn_number=qn_number)
    files_to_zip += [(orig_name, new_name)]

sp.run("rm -rf submission/files_to_zip".split())
sp.run("mkdir -p submission/files_to_zip".split())
for o, n in files_to_zip:
    sp.run("cp {} submission/files_to_zip/{}".format(o, n).split())

os.chdir('./submission/files_to_zip')
sp.run("zip {zip_folder_name}".format(zip_folder_name=zip_folder_name).split()
       + [n for o, n in files_to_zip])
sp.run("mv {} ../".format(zip_folder_name).split())

